-- Estas son las tablas del performance logger
-- `performance_world`--
-- Esta tabla guarda la info precisa de lo necesario para
-- tener un log del performance
DROP TABLE IF EXISTS `performancelog`;
CREATE TABLE `performancelog` (
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `realmId` INT(10) UNSIGNED NULL DEFAULT '0',
    `time` INT(10) UNSIGNED NULL DEFAULT '0',
    `average` INT(10) UNSIGNED NULL DEFAULT '0',
    `max` INT(10) UNSIGNED NULL DEFAULT '0',
    `players` INT(10) UNSIGNED NULL DEFAULT '0',
    `uptime` INT(10) UNSIGNED NULL DEFAULT '0',
    PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
