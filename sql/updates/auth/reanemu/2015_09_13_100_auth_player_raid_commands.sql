-- WowRean <http://www.wowrean.es>
-- Evef <https://www.github.com/Evef>

-- Adding to rbac_permissions

DELETE FROM `rbac_permissions` WHERE `id` IN(1100, 1101, 1102, 1103, 1104);

INSERT INTO `rbac_permissions` (`id`, `name`) VALUES 
('1100', 'Command: raid'),
('1101', 'Command: raid info'),
('1102', 'Command: raid list'),
('1103', 'Command: player'),
('1104', 'Command: player info');

-- Adding to player commands by default

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` IN(1100, 1101, 1102, 1103, 1104);

INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES 
('160', '1100'),
('160', '1101'),
('160', '1102'),
('160', '1103'),
('160', '1104');
