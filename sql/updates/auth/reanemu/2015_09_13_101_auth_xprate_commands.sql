-- WowRean <http://www.wowrean.es>
-- Evef <https://www.github.com/Evef>

-- Adding to rbac_permissions

DELETE FROM `rbac_permissions` WHERE `id` IN(1105, 1106, 1107, 1108, 1109);

INSERT INTO `rbac_permissions` (`id`, `name`) VALUES 
('1105', 'Command: xprate'),
('1106', 'Command: xprate kill'),
('1107', 'Command: xprate quest'),
('1108', 'Command: xprate explore'),
('1109', 'Command: xprate defaults');

-- Adding to player commands by default

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` IN(1105, 1106, 1107, 1108, 1109);

INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES 
('160', '1105'),
('160', '1106'),
('160', '1107'),
('160', '1108'),
('160', '1109');
