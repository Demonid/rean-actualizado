ALTER TABLE `gm_ticket`
    ADD COLUMN `responsedByGuid` int(10) unsigned NOT NULL DEFAULT '0' AFTER `comment`,
    ADD COLUMN `responsedByName` varchar(12) NOT NULL AFTER `responsedByGuid`;
