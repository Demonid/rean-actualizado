DROP TABLE IF EXISTS `gm_command_log`;
CREATE TABLE `gm_command_log` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logTime` int(10) unsigned NOT NULL DEFAULT '0',
  `gmAccId` int(10) unsigned NOT NULL DEFAULT '0',
  `gmCharGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `gmCharName` varchar(100) NOT NULL DEFAULT '',
  `targetType` varchar(12) NOT NULL DEFAULT '',
  `targetAccId` int(10) unsigned NOT NULL DEFAULT '0',
  `targetCharGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `targetCharName` varchar(100) NOT NULL DEFAULT '',
  `command` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
