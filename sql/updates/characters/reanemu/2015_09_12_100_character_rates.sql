-- Character rates
-- Esta tabla es para guardar el estado de la experiencia del pj y no se pierda
-- aunque reloguee, la mayor�a de servers lo tienen sin guardado
DROP TABLE IF EXISTS `character_rates`;
CREATE TABLE `character_rates` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `xp_kill` float unsigned NOT NULL DEFAULT '1',
  `xp_quest` float unsigned NOT NULL DEFAULT '1',
  `xp_explore` float unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
