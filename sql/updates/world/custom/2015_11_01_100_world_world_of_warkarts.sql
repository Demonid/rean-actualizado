-- Evento World of WarKarts
-- Soporte para GameObject y NPCs especiales

-- ScriptNmae gameobject custom ID 700011 --> Puerta de inicio
UPDATE `gameobject_template` SET `ScriptName` = 'go_warkarts_gate' WHERE `entry` IN ('700011');

-- ScriptNmae gameobject custom ID 100050 --> vehicle SLR McLaren 
UPDATE `creature_template` SET `ScriptName` = 'warkart_vehicle' WHERE `entry` IN ('100050');
