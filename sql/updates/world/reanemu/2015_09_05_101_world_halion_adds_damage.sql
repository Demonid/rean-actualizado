-- WoWRean <http://www.wowrean.es/>
-- Evef <https://github.com/Evef>

-- A�adido damage a los adds de Halion en modo hero, hasta ahora hacian 1 de da�o.

UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '4', `BaseAttackTime` = '1500' WHERE `entry` IN (40681,40683);
UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '7', `BaseAttackTime` = '1500' WHERE `entry` IN (40682,40684);
