-- WoWRean <http://www.wowrean.es/>
-- Evef <https://github.com/Evef>

UPDATE `creature_template` SET `modelid1`=169, `modelid2`=11686, `flags_extra` = `flags_extra` | 128 WHERE `entry` IN (40091, 40081, 43280, 43281, 43282, 40470, 40471, 40472);
