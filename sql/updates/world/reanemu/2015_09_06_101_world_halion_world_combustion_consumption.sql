-- WoWRean (http://www.wowrean.es)
-- Evef (https://www.github.com/Evef)

-- Linkear spells Combustion y Consumption con script
DELETE FROM `spell_script_names` WHERE spell_id IN('74630','75882','75883','75884','74802','75874','75875','75876');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
('74630', 'spell_halion_combustion_consumption_area'),
('75882', 'spell_halion_combustion_consumption_area'),
('75883', 'spell_halion_combustion_consumption_area'),
('75884', 'spell_halion_combustion_consumption_area'),
('74802', 'spell_halion_combustion_consumption_area'),
('75874', 'spell_halion_combustion_consumption_area'),
('75875', 'spell_halion_combustion_consumption_area'),
('75876', 'spell_halion_combustion_consumption_area');
