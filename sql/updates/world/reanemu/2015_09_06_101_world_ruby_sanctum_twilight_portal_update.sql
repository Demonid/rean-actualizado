-- WoWRean <http://www.wowrean.es>
-- Saitama <https://www.github.com/cagoncevatt>

-- Update fields from Twilight Portal from phase three of Halion
UPDATE `gameobject_template` SET `faction` = '35', `flags` = '32', `ScriptName` = 'go_twilight_portal' WHERE `entry` = '202795';
