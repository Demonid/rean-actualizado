-- WoWRean (http://www.wowrean.es)
-- Evef (https://www.github.com/Evef)

-- Linkear spells Fiery Combustion y Soul Consumption con script
DELETE FROM `spell_script_names` WHERE `spell_id` IN('74607','74799');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
('74607', 'spell_halion_combustion_consumption_explosion'),
('74799', 'spell_halion_combustion_consumption_explosion');
