-- WoWRean (http://www.wowrean.es)
-- Evef (https://www.github.com/Evef)

-- Halion 10n
UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '27', `BaseAttackTime` = '1500'  WHERE `entry` IN('39863','40142');
-- Halion 25n
UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '50', `BaseAttackTime` = '1500' WHERE `entry` IN('39864','40143');
-- Halion 10h
UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '42', `BaseAttackTime` = '1500' WHERE `entry` IN('39944','40144');
-- Halion 25h
UPDATE `creature_template` SET `BaseVariance` = '10' , `DamageModifier` = '74', `BaseAttackTime` = '1500' WHERE `entry` IN('39945','40145');
