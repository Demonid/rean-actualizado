-- Eilo (https://github.com/eilo)
-- XpRate Commands

-- A�adiendo la base que son los comandos a usar ingame
DELETE FROM `command` WHERE `name`LIKE 'xprate';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('xprate','1105','Sintaxis: .xprate #n \r\nConfigura nuestra experiencia de matar bichos, exploracion y misione al valor escrito!\r\nEl valor no podra exceder los rates del server.\r\n\nWowRean');
DELETE FROM `command` WHERE `name`LIKE 'xprate defaults';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('xprate defaults','1109','Sintaxis: .xprate defaults\r\nConfigura la experiencia a los valores por defecto del servidor.\r\n\nWowRean');
DELETE FROM `command` WHERE `name`LIKE 'xprate kill';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('xprate kill','1106','Sintaxis: .xprate kill #n\r\nConfigura la experiencia al matar bichos.\r\n\nWowRean');
DELETE FROM `command` WHERE `name`LIKE 'xprate quest';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('xprate quest','1107','Sintaxis: .xprate quest #n\r\nConfigura la experiencia al completar misiones.\r\n\nWowRean');
DELETE FROM `command` WHERE `name`LIKE 'xprate explore';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('xprate explore','1108','Sintaxis: .xprate explore #n\r\nConfigura la experiencia al explorar zonas.\r\n\nWowRean');


-- Strings
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 11350 AND 11358;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(11350, '|cff00ffff=========== XP RATES ============', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11351, '|Cff00ff00 %s|r tus rates han sido configurados a:', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11352, '|Cff00ff00 Kill:|r x%f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11353, '|Cff00ff00 Quest:|r x%f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11354, '|Cff00ff00 Explore:|r x%f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11355, '  - WowRean - Puedes usar .xprate #n para modificar los rates juntos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11356, '  - WowRean - Puedes usar .xprate kill #n .xprate quest #n o .xprate explore #n para modificar por separado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11357, '  - WowRean - Puedes usar .xprate defaults para volver a los rates globales del server.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11358, '|cff00ffff=================================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
