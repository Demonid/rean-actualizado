-- Battlegrounds Blizzlike Chance
UPDATE `battleground_template` SET `Weight` = '8' WHERE `id` = '2'; -- Warsong Gulch
UPDATE `battleground_template` SET `Weight` = '7' WHERE `id` = '7'; -- Eye of The Storm
UPDATE `battleground_template` SET `Weight` = '6' WHERE `id` = '3'; -- Arathi Basin
UPDATE `battleground_template` SET `Weight` = '1' WHERE `id` = '1'; -- Alterac Valley
UPDATE `battleground_template` SET `Weight` = '2' WHERE `id` = '9'; -- Strand of the Ancients 
UPDATE `battleground_template` SET `Weight` = '2' WHERE `id` = '30'; -- Isle of Conquest 
