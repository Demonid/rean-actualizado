-- Bladestorm inmune a Cyclone
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=46924 AND `spell_effect`=-33786 ;
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(46924, -33786, 2, 'Bladestorm immune Cyclone(Druid)');
