-- WoWRean <http://www.wowrean.es/>
-- Evef <https://github.com/Evef>

DELETE FROM `conditions` WHERE `SourceEntry` IN(75888, 75889);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`) VALUES 
('17', '75888', '31', '3', '40683'),
('17', '75889', '31', '3', '40684');
