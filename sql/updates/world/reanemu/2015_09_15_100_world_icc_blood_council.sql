-- BloodCouncil: Add Immunities to Kinectic Bomb
UPDATE creature_template SET mechanic_immune_mask = 650854271 WHERE entry IN (38454,38775,38776,38777);

-- BloodCouncil: Add Immunities to Dark Nucleus
UPDATE creature_template SET mechanic_immune_mask = 650854271 WHERE entry = 38369;
