-- Deathbringer Saurfang: Update Flags on Triggers
UPDATE `creature_template` SET unit_flags = 33554432, flags_extra = 128 WHERE entry IN (39010, 39011, 39012, 39013);
