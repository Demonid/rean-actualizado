-- Lady Deathwhisper: Vengeful Shade Movement Speed
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry` IN (38222);

-- Lady Deathwhisper: delete spell script Cultist Dark Martyrdom
DELETE FROM `spell_script_names` WHERE `spell_id` IN (70903, 71236, 72495, 72496, 72497, 72498, 72499, 72500) AND `ScriptName` = "spell_cultist_dark_martyrdom";
