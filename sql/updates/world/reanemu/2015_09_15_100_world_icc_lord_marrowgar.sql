-- Lord Marrowgar: Set inhabit type for coldflame npc trigger
UPDATE `creature_template` SET `InhabitType` = 1 WHERE `entry` = 36672;

-- Lord Marrowgar: Set bone spike immunities
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36619,38233,38459,38460);
