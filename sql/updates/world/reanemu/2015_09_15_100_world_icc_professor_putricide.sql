-- Professor Putricide: Excluyendo a la abominacion del efecto del stun del choking gas bomb
DELETE FROM `spell_script_names` WHERE spell_id IN (71278, 72460, 72619, 72620, 71279, 72459, 72621, 72622);
INSERT INTO `spell_script_names` (spell_id, ScriptName) VALUES
(71278, 'spell_putricide_choking_gas_bomb_effect'),
(72460, 'spell_putricide_choking_gas_bomb_effect'),
(72619, 'spell_putricide_choking_gas_bomb_effect'),
(72620, 'spell_putricide_choking_gas_bomb_effect'),
(71279, 'spell_putricide_choking_gas_bomb_effect'),
(72459, 'spell_putricide_choking_gas_bomb_effect'),
(72621, 'spell_putricide_choking_gas_bomb_effect'),
(72622, 'spell_putricide_choking_gas_bomb_effect');

-- Professor Putricide Tear Gas for enemies
DELETE FROM `spell_script_names` WHERE `spell_id` = 71615 AND `scriptName` LIKE 'spell_putricide_tear_gas';
INSERT INTO `spell_script_names` VALUES (71615, 'spell_putricide_tear_gas');

-- Professor Putricide: Update Immunities for Volatile Ooze and Gas Gloud
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37697,38604,38758,38759); -- Volatile Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37562,38602,38760,38761); -- Gas Cloud

-- Professor Putricide: Update for Volatile Ooze and Gas Cloud
UPDATE `creature_template` SET `speed_walk`=0.68 WHERE `entry` IN (37697,38604,38758,38759); -- Volatile Ooze
UPDATE `creature_template` SET `speed_walk`=0.68 WHERE `entry` IN (37562,38602,38760,38761); -- Gas Cloud
