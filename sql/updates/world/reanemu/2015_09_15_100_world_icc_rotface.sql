-- Rotface: Condition for Vile Gas
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` IN (72272, 72273);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','4','72272','0','0','31','0','4','0','0','0','0','0','','vile gas player only target'),
('13','1','72272','0','0','31','0','4','0','0','0','0','0','','vile gas player only target'),
('13','2','72272','0','0','31','0','4','0','0','0','0','0','','vile gas player only target');

-- Rotface: Add immunities to Rotface
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36627,38390,38549,38550);

-- Rotface: Add immunities to Little Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36897,38138);

-- Rotface: Add immunities to Big Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36899,38123);

-- Rotface: Make Big Ooze slower
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry` IN (36899, 38123);
