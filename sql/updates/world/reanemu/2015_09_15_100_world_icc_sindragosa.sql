-- Sindragosa - Ice Tomb resistance
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = '70157' AND `spell_effect` = '69700' AND `type` = '2' AND `comment` = 'Sindragosa - Ice Tomb resistance';

-- Sindragosa: Fix spell 69762 Unchained Magic - Add internal cooldown with 1 seconds, so multi-spell spells will only apply one stack of triggered spell 69766 (i.e. Chain Heal)
DELETE FROM `spell_proc_event` WHERE `entry` = 69762;
INSERT INTO `spell_proc_event` (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(69762, 0, 0, 0, 0, 0, 81920, 0, 0, 0, 1);

-- Sindragosa: Set InhabitType = 4
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (36853,38265,38266,38267); -- Sindragosa
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37533,38220); -- Rimefang
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37534,38219); -- Spinestalker

-- Sindragosa: Update Immunities for Rimefang, Spinestalker and Ice Tomb
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37533,38220); -- Rimefang
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37534,38219); -- Spinestalker
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36980, 38320, 38321, 38322); -- Ice Tomb
