-- Valithria Dreamwalker: Lich King missing text
DELETE FROM `creature_text` WHERE `entry`=16980;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(16980,0,0,"Intruders have breached the inner sanctum! Hasten the destruction of the green dragon, leave only bones and sinew for the reanimation!",14,0,0,0,0,17251,"Valithria Dreamwalker - SAY_LICH_KING_INTRO");

-- Valithria Dreamwalker: only 2 equip items should drop
UPDATE `gameobject_loot_template` SET `maxcount` = 2 WHERE `entry` = 28052 AND `reference` = 34241;
