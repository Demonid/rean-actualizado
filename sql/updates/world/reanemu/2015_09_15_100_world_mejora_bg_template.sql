-- Pesos en bg para mejor jugabilidad y cambio de minimo maximo por faccion
-- Alterac 30%
UPDATE `battleground_template` SET `weight`=3 WHERE `id`=1;
-- Grito de Guerra 80%
UPDATE `battleground_template` SET `weight`=8 WHERE `id`=2;
-- Arathi 60%
UPDATE `battleground_template` SET `weight`=6 WHERE `id`=3;
-- Ojo de tormenta 70%
UPDATE `battleground_template` SET `weight`=7 WHERE `id`=7;
-- Playa de ancestros 20%
UPDATE `battleground_template` SET `weight`=2 WHERE `id`=9;
-- Isla de conquista 10%
UPDATE `battleground_template` SET `weight`=1 WHERE `id`=30;
