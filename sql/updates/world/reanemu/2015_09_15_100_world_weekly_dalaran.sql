-- Make Lan'Dalock's weekly quests not sharable
UPDATE `quest_template` SET `flags`=flags&~8 WHERE `id` IN ('24579','24580','24581','24582','24583','24584','24585','24586','24587','24588','24589','24590');
