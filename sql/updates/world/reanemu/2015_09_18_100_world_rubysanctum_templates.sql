-- WoWRean (http://www.wowrean.es)
-- Evef (https://www.github.com/Evef)
-- Eilo (https://www.github.com/eilo)
-- Instance: Ruby Sanctum

-- Immunity
UPDATE `creature_template` SET `mechanic_immune_mask` = `mechanic_immune_mask` | 
1|          -- charm
2|          -- disorient
4|          -- disarm
8|          -- distract
16|         -- fear
32|         -- grip
64|         -- root
256|        -- silence
512|        -- sleep
1024|       -- snare
2048|       -- stun
4096|       -- freeze
8192|       -- knockout
65536|      -- polymorph
131072|     -- banish
524288|     -- shackle
1048576|    -- mount
4194304|    -- turn
8388608|    -- horror
33554432|   -- interrupt
67108864|   -- daze
536870912   -- sapped
where `entry` IN (
39747, 39823   -- Saviana Ragefire
);

UPDATE `creature_template` SET `mechanic_immune_mask` = `mechanic_immune_mask` | 
1|          -- charm
2|          -- disorient
4|          -- disarm
8|          -- distract
16|         -- fear
32|         -- grip
64|         -- root
128|        -- pacify
256|        -- silence
512|        -- sleep
1024|       -- snare
2048|       -- stun
4096|       -- freeze
8192|       -- knockout
65536|      -- polymorph
131072|     -- banish
524288|     -- shackle
1048576|    -- mount
4194304|    -- turn
8388608|    -- horror
33554432|   -- interrupt
67108864|   -- daze
536870912   -- sapped
where `entry` IN (
39751, 39920,   -- Baltharus the Warborn
39899, 39922,   -- Baltharus the Warborn Clone
39746, 39805,    -- General Zarithrian
39863, 39864, 39944, 39945, -- Halion
40142, 40143, 40144, 40145 -- Twilight Halion
);

-- Bosses spawntime & corpse remove time
UPDATE `creature` SET `spawntimesecs` = 604800 WHERE `id` IN (39863,39751,39746,39747);
UPDATE `creature_template` SET `rank` = 3 WHERE `entry` IN  (39863,39751,39746,39747);

-- Trash spawntime
UPDATE `creature` SET `spawntimesecs`=1209600 WHERE `map`=724 AND `id` NOT IN (39863,39751,39746,39747);

-- Ruby Sanctum bosses id saving
UPDATE `creature_template` SET `flags_extra` = 1 WHERE `entry` IN
(39863,39864,39944,39945, -- Halion
39751,39920, -- Baltharus
39746,39805, -- General Zarithrian
39747,39823); -- Saviana Ragefire
