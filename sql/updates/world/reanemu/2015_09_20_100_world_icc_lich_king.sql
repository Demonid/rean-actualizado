-- Lich King: Update Immunities for Shambling Horror, Drudge Ghoul
UPDATE `creature_template` SET mechanic_immune_mask = 650851199 WHERE entry IN (37698,39299,39300,39301); -- Shambling Horror
UPDATE `creature_template` SET mechanic_immune_mask = 650851199 WHERE entry IN (37695,39309,39310,39311); -- Drudge Ghoul

-- Lich King: Update Immunities for Ice Sphere
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36633,39305,39306,39307); -- Ice Sphere

-- Lich King: Update Immunities for Raging Spirit
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36701,39302,39303,39304); -- Raging Spirit

-- Lich King: Update Immunities and Speed for Valkyr Shadowguard
UPDATE creature_template SET mechanic_immune_mask = 2145367039 WHERE entry IN (36609, 39120, 39121, 39122); -- Valkyr Shadowguard
UPDATE creature_template SET speed_walk = 0.642857, speed_run =0.642857 WHERE `entry` IN (36609, 39120, 39121, 39122); -- Valkyr Shadowguard

-- Lich King: Update Immunities and speed for Vile Spirits
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (37799, 39284, 39285, 39286); -- Vile Spirits
UPDATE creature_template SET speed_walk = 0.5, speed_run = 0.5 WHERE `entry` IN (37799, 39284, 39285, 39286); -- Vile Spirits

-- Lich King: Update Immunities and speed for Wicked Spirit
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (39190, 39287, 39288, 39289); -- Wicked Spirit
UPDATE creature_template SET speed_walk = 0.5, speed_run = 0.5 WHERE `entry` IN (39190, 39287, 39288, 39289); -- Wicked Spirit

-- Lich King: Update Immunities and speed for Spirit Bomb
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry =39189; -- Spirit Bomb
UPDATE `creature_template` SET speed_walk = 0.5, speed_run = 0.5 WHERE entry =39189; -- Spirit Bomb

-- The Lich King: Wicked Spirit Scriptname
UPDATE `creature_template` SET `ScriptName` = 'npc_wicked_spirit' WHERE `entry` = 39190;
UPDATE `creature_template` SET `unit_flags` = `unit_flags` |512 WHERE `entry` IN (39190, 39287, 39288, 39289);

-- Lich King: 25HC Updating Loot
UPDATE `creature_loot_template` SET `maxcount` = '2' WHERE `entry` = '39168' AND `item` = '1';
UPDATE `creature_loot_template` SET `maxcount` = '3' WHERE `entry` = '39168' AND `item` = '2';
