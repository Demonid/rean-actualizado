-- ----------------------------
--    Quel'delar phasing
-- ----------------------------

SET @PHASE := 70193;

DELETE FROM `spell_area` WHERE `spell`=@PHASE;
INSERT INTO `spell_area` (`spell`,`area`,`quest_start`,`quest_start_status`,`quest_end`,`quest_end_status`,`aura_spell`,`racemask`,`gender`,`autocast`) VALUES
-- Alliance
(@PHASE,4080,24522,74,24553,11,0,1101,2,1),
(@PHASE,4075,24522,74,24553,11,0,1101,2,1),
-- Horde, non bloodelves
(@PHASE,4080,24562,74,24564,11,0,178,2,1),
(@PHASE,4075,24562,74,24564,11,0,178,2,1),
-- Blood elf
(@PHASE,4080,24562,74,24594,11,0,512,2,1),
(@PHASE,4075,24562,74,24594,11,0,512,2,1);
