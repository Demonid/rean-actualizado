-- Update  Wintergrasp vehicle teleporter
DELETE FROM `gameobject` WHERE id IN (192951) AND map = 571;
-- Triggers derecha
UPDATE `creature` SET `position_x` = '5299.16' , `position_y` = '3028.84' , `position_z` = '412.146' , `orientation` = '5.37561' WHERE `guid` = '131722'; 
UPDATE `creature` SET `position_x` = '5255.89' , `position_y` = '2970.04' , `position_z` = '409.274' , `orientation` = '3.08923' WHERE `guid` = '131721'; 

-- Triggers Izquierda
UPDATE `creature` SET `position_x` = '5296.89' , `position_y` = '2651.11' , `position_z` = '413.405' , `orientation` = '0.750492' WHERE `guid` = '131707'; 
UPDATE `creature` SET `position_x` = '5255.89' , `position_y` = '2710.69' , `position_z` = '409.275' , `orientation` = '3.07178' WHERE `guid` = '131724';
