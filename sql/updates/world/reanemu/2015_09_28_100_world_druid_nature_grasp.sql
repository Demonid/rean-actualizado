-- Nature Grasp ICD 2 Seconds
DELETE FROM `spell_proc_event` WHERE `entry` IN (16689,16810,16811,16812,16813,17329,27009,53312);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`)VALUES 
(16689, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16810, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16811, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16812, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16813, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(17329, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(27009, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(53312, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2);
