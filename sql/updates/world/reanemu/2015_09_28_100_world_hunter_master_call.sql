-- Removes Master's Call stun immunity.
DELETE FROM `spell_linked_spell` WHERE spell_trigger = 54216;
INSERT INTO `spell_linked_spell`(`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (54216,-56651,1,'Removes Master''s Call stun immunity');
