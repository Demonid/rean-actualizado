-- Encantamientos varios eliminar exploits PvP
DELETE FROM `spell_bonus_data` WHERE `entry` IN (6297,20004,28005,20006,44525);
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
-- Enchants
(6297,0,0,0,0,'Enchant - Fiery Blaze'),
(20004,0,0,0,0,'Enchant - Lifestealing'),
(28005,0,0,0,0,'Enchant - Battlemaster'),
(20006,0,0,0,0,'Enchant - Unholy Weapon'),
(44525,0,0,0,0,'Enchant - Icebreaker');
