DELETE FROM `spell_bonus_data` WHERE `entry` IN (10577,27655,28788,38395,55756,28715,38616,43731,43733,30567,14795);
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
-- Items
(10577,0,0,0,0,'Item - Gauntlets of the Sea (Heal)'),
(27655,0,0,0,0,'Item - Heart of Wyrmthalak (Flame Lash)'),
(28788,0,0,0,0,'Item - Paladin T3 (8)'),
(38395,0,0,0,0,'Item - Warlock T5 (2)'),
(55756,0,0,0,0,'Item - Brunnhildar weapons (Chilling Blow)'),
-- Consumables
(28715,0,0,0,0,'Consumable - Flamecap (Flamecap Fire)'),
(38616,0,0,0,0,'Poison - Bloodboil Poison'),
(43731,0,0,0,0,'Consumable - Stormchops (Lightning Zap)'),
(43733,0,0,0,0,'Consumable - Stormchops (Lightning Zap)'),
(30567,0,0,0,0,'Consumable - Torment of the Worgen'),
(14795,0,0,0,0,'Poison - Venomhide Poison Debuff - should not get bonuses');
