-- Fix Mage Hot Streak, ahora se elimina al lanzar la spell y no al impactar con el objetivo, as� se previene exploit de doble cast.

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (11366,12505,12522,12523,12524,12525,12526,18809,27132,33938,42890,42891);
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(11366,-48108,0,'Pyroblast (Rank 1)'),
(12505,-48108,0,'Pyroblast (Rank 2)'),
(12522,-48108,0,'Pyroblast (Rank 3)'),
(12523,-48108,0,'Pyroblast (Rank 4)'),
(12524,-48108,0,'Pyroblast (Rank 5)'),
(12525,-48108,0,'Pyroblast (Rank 6)'),
(12526,-48108,0,'Pyroblast (Rank 7)'),
(18809,-48108,0,'Pyroblast (Rank 8)'),
(27132,-48108,0,'Pyroblast (Rank 9)'),
(33938,-48108,0,'Pyroblast (Rank 10)'),
(42890,-48108,0,'Pyroblast (Rank 11)'),
(42891,-48108,0,'Pyroblast (Rank 12)');
