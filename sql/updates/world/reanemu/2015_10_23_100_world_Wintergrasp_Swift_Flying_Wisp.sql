-- Allow Swift Flying Wisp in Wintergrasp
DELETE FROM `spell_area` WHERE `spell`='55173' AND `area`='4197';
INSERT INTO `spell_area` (`spell`, `area`, `quest_start`, `quest_end`, `aura_spell`, `racemask`, `gender`, `autocast`, `quest_start_status`, `quest_end_status`) VALUES
('55173','4197','0','0','8326','8','2','1','64','11');
