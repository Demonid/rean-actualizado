-- Arena Espectador WowRean

-- Emotes are not allowed
DELETE FROM `trinity_string` WHERE `entry` = 12000;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(12000, 'No puedes hacer eso mientras eres espectador.');

-- Arena spectator npc
DELETE FROM creature_template WHERE entry = '190000'; 
INSERT INTO creature_template (entry, modelid1, NAME, subname, IconName, gossip_menu_id, minlevel, maxlevel, HealthModifier, ManaModifier, ArmorModifier, faction, npcflag, speed_walk, speed_run, scale, rank, DamageModifier, unit_class, unit_flags, TYPE, type_flags, InhabitType, RegenHealth, flags_extra, ScriptName) VALUES 
('190000', '729', "Arena Espectador", "WoW Rean", 'Speak', '50000', 71, 71, 1.56, 1.56, 1.56, 35, 3, 1, 1.14286, 1.25, 1, 1, 1, 2, 7, 138936390, 3, 1, 2, 'npc_arena_espectador');

-- Spawn en Ventor y Orgri al lado del transfiguración
DELETE FROM `creature` WHERE `id` IN (190000);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
('2673267','190000','0','1','1','0','0','-8823.95','638.014','94.235','2.35341','300','0','0','6494','0','0','0','0','0'),
('2672453','190000','1','1','1','0','0','1589.78','-4422.17','8.9641','1.90141','300','0','0','6494','0','0','0','0','0');
