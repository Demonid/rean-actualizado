-- Pilgrim's Paunch Spell scripts
DELETE FROM `spell_script_names` WHERE `spell_id` IN (61804, 61805, 61806, 61807, 61808);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
('61804', 'Spell_Pilgrim_Paunch'),
('61805', 'Spell_Pilgrim_Paunch'),
('61806', 'Spell_Pilgrim_Paunch'),
('61807', 'Spell_Pilgrim_Paunch'),
('61808', 'Spell_Pilgrim_Paunch');
