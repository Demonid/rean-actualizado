-- Evento del Mundo - Generosidad del Peregrino

-- Pilgrim's Paunch Spell scripts
DELETE FROM `spell_script_names` WHERE `spell_id` IN (61804, 61805, 61806, 61807, 61808);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
('61804', 'Spell_Pilgrim_Paunch'),
('61805', 'Spell_Pilgrim_Paunch'),
('61806', 'Spell_Pilgrim_Paunch'),
('61807', 'Spell_Pilgrim_Paunch'),
('61808', 'Spell_Pilgrim_Paunch');

-- Añadidos criteria data
DELETE FROM `achievement_criteria_data` WHERE
 `criteria_id` BETWEEN 11118 AND 11127 OR -- Now We're Cookin'
 `criteria_id` BETWEEN 11134 AND 11141 OR -- Pilgrim's Peril
 `criteria_id` BETWEEN 11078 AND 11085;   -- Pilgrim's Paunch
INSERT INTO achievement_criteria_data (`criteria_id`,`type`,`value1`,`value2`,`ScriptName`) VALUES
(11078,14,469,0,''),
(11079,14,469,0,''),
(11080,14,469,0,''),
(11081,14,469,0,''),
(11082,14,67,0,''),
(11083,14,67,0,''),
(11084,14,67,0,''),
(11085,14,67,0,''),
(11118,14,469,0,''),
(11119,14,469,0,''),
(11120,14,469,0,''),
(11121,14,469,0,''),
(11122,14,469,0,''),
(11123,14,67,0,''),
(11124,14,67,0,''),
(11126,14,67,0,''),
(11127,14,67,0,''),
(11134,14,469,0,''),
(11135,14,469,0,''),
(11136,14,469,0,''),
(11137,14,469,0,''),
(11138,14,67,0,''),
(11139,14,67,0,''),
(11140,14,67,0,''),
(11141,14,67,0,'');

-- Logro: Turkey Lurkey
DELETE FROM `achievement_criteria_data` WHERE `type`=2 AND `criteria_id` IN (11158,11159,11160,11161,11162,11163,11164,11165);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
(11158, 2, 4, 1), -- Human Rogue
(11159, 2, 4, 4), -- Night Elf Rogue
(11160, 2, 4, 2), -- Orc Rogue
(11161, 2, 4, 8), -- Troll Rogue
(11162, 2, 4, 5), -- Undead Rogue
(11163, 2, 4, 10), -- Blood Elf Rogue
(11164, 2, 4, 3), -- Dwarf Rogue
(11165, 2, 4, 7); -- Gnome Rogue

-- fix sillas
UPDATE `creature_template` SET `VehicleId` = '321' WHERE `entry` IN (34823,34812,34824,34822,34819); -- Temp Fix (not blizz)

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` IN (34823,34812,34824,34822,34819);
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
('34823', '65403', '1', '0'), -- The Cranberry Chair
('34812', '65403', '1', '0'), -- The Turkey Chair
('34824', '65403', '1', '0'), -- The Sweet Potato Chair
('34822', '65403', '1', '0'), -- The Pie Chair
('34819', '65403', '1', '0'); -- The Stuffing Chair

-- Fix a few buycounts
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=44853; -- Honey
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=44835; -- Autumnal Herbs
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=44854; -- Tangy Wetland Cranberries
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=44855; -- Teldrassil Sweet Potato
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=46797; -- Mulgore Sweet Potato
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=46793; -- Tangy Southfury Cranberries
UPDATE `item_template` SET `BuyCount`=5 WHERE `entry`=46796; -- Ripe Trisfal Pumpkin

-- Auras for chairs
DELETE FROM `creature_template_addon` WHERE `entry` IN (34823,34812,34824,34822,34819);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
('34823', '0', '0', '0', '1', '0', '61793 61798'), -- The Cranberry Chair / (Cranberry Server | Can Eat - Cranberries)
('34812', '0', '0', '0', '1', '0', '61796 61801'), -- The Turkey Chair / (Turkey Server | Can Eat - Turkey)
('34824', '0', '0', '0', '1', '0', '61797 61802'), -- The Sweet Potato Chair / (Sweet Potatoes Server | Can Eat - Sweet Potatoes)
('34822', '0', '0', '0', '1', '0', '61794 61799'), -- The Pie Chair / (Pie Server | Can Eat - Pie)
('34819', '0', '0', '0', '1', '0', '61795 61800'); -- The Stuffing Chair / (Stuffing Server | Can Eat - Stuffing)

-- Pilgrims Bounty: Chair
UPDATE creature_template SET spell1=66261, spell2=61784, spell3=61785, spell4=61788, spell5=61786, spell6=61787 WHERE entry=34823; -- The Cranberry Chair
UPDATE creature_template SET spell1=66250, spell2=61784, spell3=61785, spell4=61788, spell5=61786, spell6=61787 WHERE entry=34812; -- The Turkey Chair
UPDATE creature_template SET spell1=66259, spell2=61784, spell3=61785, spell4=61788, spell5=61786, spell6=61787 WHERE entry=34819; -- The Stuffing Chair
UPDATE creature_template SET spell1=66260, spell2=61784, spell3=61785, spell4=61788, spell5=61786, spell6=61787 WHERE entry=34822; -- The Pie Chair
UPDATE creature_template SET spell1=66262, spell2=61784, spell3=61785, spell4=61788, spell5=61786, spell6=61787 WHERE entry=34824; -- The Sweet Potato Chair

-- Logro: Pilgrim's Peril
DELETE FROM `achievement_criteria_data` WHERE `type`= 16 AND `criteria_id` IN (11134,11135,11136,11137,11138,11139,11140,11141);
DELETE FROM `achievement_criteria_data` WHERE `type`= 6 AND `criteria_id` IN (11134,11135,11136,11137,11138,11139,11140,11141);
DELETE FROM `achievement_criteria_data` WHERE `type`= 5 AND `criteria_id` IN (11134,11135,11136,11137,11138,11139,11140,11141);
DELETE FROM `achievement_criteria_data` WHERE `type`= 14 AND `criteria_id` IN (11134,11135,11136,11137,11138,11139,11140,11141);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
-- Alianza
('11134', '16', '404', '0'), -- Orgrimmar
('11134', '6', '14', '0'), -- Orgrimmar
('11134', '5', '66303', '0'), -- Orgrimmar
('11135', '16', '404', '0'), -- Silvermoon City
('11135', '6', '3470', '0'), -- Silvermoon City
('11135', '5', '66303', '0'), -- Silvermoon City
('11136', '16', '404', '0'), -- Thunder Bluff
('11136', '6', '1638', '0'), -- Thunder Bluff
('11136', '5', '66303', '0'), -- Thunder Bluff
('11137', '16', '404', '0'), -- Undercity
('11137', '6', '1497', '0'), -- Undercity
('11137', '5', '66303', '0'), -- Undercity
-- Horda
('11138', '16', '404', '0'), -- Exodar
('11138', '6', '3557', '0'), -- Exodar
('11138', '5', '66303', '0'), -- Exodar
('11139', '16', '404', '0'), -- Darnassus
('11139', '6', '1657', '0'), -- Darnassus
('11139', '5', '66303', '0'), -- Darnassus
('11140', '16', '404', '0'), -- Ironforge
('11140', '6', '809', '0'), -- Ironforge
('11140', '5', '66303', '0'), -- Ironforge
('11141', '16', '404', '0'), -- Stormwind
('11141', '6', '12', '0'), -- Stormwind
('11141', '5', '66303', '0'); -- Stormwind

-- Logro: Now We're Cookin'
DELETE FROM `achievement_criteria_data` WHERE `type`= 14 AND `criteria_id` IN (11118, 11119, 11120, 11121, 11122, 11123, 11124, 11125, 11126, 11127);
DELETE FROM `achievement_criteria_data` WHERE `type`= 16 AND `criteria_id` IN (11118, 11119, 11120, 11121, 11122, 11123, 11124, 11125, 11126, 11127);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
-- Alianza
('11118','16','404','0'), -- Now We're Cookin' / Cranberry Chutney
('11119','16','404','0'), -- Now We're Cookin' / Candied Sweet Potato
('11120','16','404','0'), -- Now We're Cookin' / Pumpkin Pie
('11121','16','404','0'), -- Now We're Cookin' / Slow-Roasted Turkey
('11122','16','404','0'), -- Now We're Cookin' / Spice Bread Stuffing
-- Horda
('11123','16','404','0'), -- Now We're Cookin' / Candied Sweet Potato
('11124','16','404','0'), -- Now We're Cookin' / Cranberry Chutney
('11125','16','404','0'), -- Now We're Cookin' / Pumpkin Pie
('11126','16','404','0'), -- Now We're Cookin' / Slow-Roasted Turkey
('11127','16','404','0'); -- Now We're Cookin' / Spice Bread Stuffing

-- Logro: Terokkar Turkey Time
DELETE FROM `achievement_criteria_data` WHERE `type` = 5 AND `criteria_id` = 11142;
DELETE FROM `achievement_criteria_data` WHERE `type` = 16 AND `criteria_id` = 11142;
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
(11142, 5, 66303, 0), -- Pilgrim Costume
(11142, 16, 404, 0);

-- Logro: Pilgrim's Paunch
DELETE FROM `achievement_criteria_data` WHERE `type` = 6 AND `criteria_id` IN (11078,11079,11080,11081,11082,11083,11084,11085);
DELETE FROM `achievement_criteria_data` WHERE `type` = 16 AND `criteria_id` IN (11078,11079,11080,11081,11082,11083,11084,11085);
DELETE FROM `achievement_criteria_data` WHERE `type` = 14 AND `criteria_id` IN (11078,11079,11080,11081,11082,11083,11084,11085);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
-- Alianza
(11078, 6, 1657, 0),  -- Darnassus
(11078, 16, 404, 0),  -- Darnassus
(11079, 6, 809, 0),   -- Ironforge
(11079, 16, 404, 0),  -- Ironforge
(11080, 6, 3557, 0),  -- Exodar
(11080, 16, 404, 0),  -- Exodar
(11081, 6, 12, 0),    -- Stormwind
(11081, 16, 404, 0),  -- Stormwind
-- Horda
(11082, 6, 14, 0),    -- Orgrimmar
(11082, 16, 404, 0),  -- Orgrimmar
(11083, 6, 3470, 0),  -- Silvermoon City
(11083, 16, 404, 0),  -- Silvermoon City
(11084, 6, 1638, 0),  -- Thunder Bluff
(11084, 16, 404, 0),  -- Thunder Bluff
(11085, 6, 1497, 0),  -- Undercity
(11085, 16, 404, 0);  -- Undercity

-- Logro: "FOOD FIGHT!"
DELETE FROM `achievement_criteria_data` WHERE `type` = 11 AND `criteria_id` IN (11168,11178,11179,11180,11181);
DELETE FROM `achievement_criteria_data` WHERE `type` = 16 AND `criteria_id` IN (11168,11178,11179,11180,11181);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`, `ScriptName`) VALUES
(11168, 16, 404, 0, ''),
(11178, 16, 404, 0, ''),
(11179, 16, 404, 0, ''),
(11180, 16, 404, 0, ''),
(11181, 16, 404, 0, '');

-- Logro: Sharing is Caring
DELETE FROM `achievement_criteria_data` WHERE `criteria_id` IN (11086,11088,11089,11090,11167);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
(11086, 16, 404, 0),
(11088, 16, 404, 0),
(11089, 16, 404, 0),
(11090, 16, 404, 0),
(11167, 16, 404, 0);

UPDATE `achievement_reward` SET `item`=44810, `sender`=28951, `subject`='A Gobbler not yet Gobbled', `text` = 'Can you believe this Plump Turkey made it through November alive?$B$BSince all this friends have been served up on Bountiful Tables with sides of Cranberry Chutney and Spice Bread Stuffing and... ooo... I''m getting hungry. But anyhow! He''s all alone, now, so I was hoping you might be willing to take care of him. There simply isn''t enough room left in my shop!$B$BJust keep him away from cooking fires, please. He gets this strange look in his eyes around them...' WHERE `entry` IN (3478,3656); -- Pilgrim achievement (A/H)


-- Arreglos npcs
SET @ENTRY := 32820;
UPDATE `creature_template` SET `AIName`="SmartAI", `faction`='31' WHERE `entry`=@ENTRY;
UPDATE `creature_template` SET `VehicleId`='0' WHERE `entry`=32823;
-- Quitar el movimiento de las sillas
UPDATE `creature_template` SET `speed_run`=0 WHERE  `entry`=34812;
UPDATE `creature_template` SET `speed_run`=0 WHERE  `entry`=34823;
UPDATE `creature_template` SET `speed_run`=0 WHERE  `entry`=34822;
UPDATE `creature_template` SET `speed_run`=0 WHERE  `entry`=34824;
UPDATE `creature_template` SET `speed_run`=0 WHERE  `entry`=34819;
DELETE FROM `creature_template_addon` WHERE (`entry`=34812);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (34812, 0, 0, 0, 1, 0, '61796 61801 38505');
DELETE FROM `creature_template_addon` WHERE (`entry`=34822);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (34822, 0, 0, 0, 1, 0, '61794 61799 38505');
DELETE FROM `creature_template_addon` WHERE (`entry`=34824);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (34824, 0, 0, 0, 1, 0, '61797 61802 38505');
DELETE FROM `creature_template_addon` WHERE (`entry`=34819);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (34819, 0, 0, 0, 1, 0, '61795 61800 38505');
DELETE FROM `creature_template_addon` WHERE (`entry`=34823);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (34823, 0, 0, 0, 1, 0, '61793 61798 38505');


DELETE FROM `creature_template` WHERE `entry`IN (100100);
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES 
(100100, 0, 0, 0, 0, 0, 21774, 0, 0, 0, 'Wild Turkey', '', '', 0, 1, 1, 0, 14, 0, 1, 1.14286, 1, 0, 0, 2000, 2000, 1, 1, 1, 0, 2048, 0, 0, 0, 0, 0, 0, 1, 0, 32820, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 0.05, 1, 1, 1, 1, 0, 121, 1, 0, 0, '', 12340);
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=100100;
-- Cambio facción misiones horda
UPDATE `quest_template` SET `AllowableRaces`=690 WHERE  `Id` IN (14047,14037,14044,14041,14043,14062,14061,14060,14059,14058,14040,14036,14065);

-- Cambio facción misiones alianza
UPDATE `quest_template` SET `AllowableRaces`=1101 WHERE  `Id` IN (14022,14064,14035,14023,14055,14030,14024,14028,14033,14051,14048,14054,14053);

-- Wild Turkey SAI
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`='100100' AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,6,0,100,0,0,0,0,0,75,62014,0,0,0,0,0,7,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,1,0,6,0,100,0,60,0,0,0,85,62021,2,0,0,0,0,7,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,6,0,100,0,0,0,0,0,12,100100,6,0,0,0,0,7,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(100100,0,0,0,24,0,100,0,62014,39,1,2,75,62021,2,0,0,0,0,21,100,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(100100,0,1,0,6,0,100,0,0,0,0,0,75,62014,0,0,0,0,0,7,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(100100,0,2,0,6,0,100,0,60,0,0,0,85,62021,2,0,0,0,0,7,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` IN (22) and `SourceId`=0 and `SourceEntry` IN (@ENTRY,100100);
INSERT INTO `conditions`(`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorType`,`ErrorTextId`,`ScriptName`,`Comment`) values
(22,2,@ENTRY,0,0,1,0,62021,0,0,0,0,0,'','BOX - activate SAI if invoker is SRL McLaren'),
(22,3,@ENTRY,0,0,16,0,1791,0,0,0,0,0,'','BOX - activate SAI if invoker is SRL McLaren'),
(22,3,@ENTRY,0,0,1,0,62021,0,0,1,0,0,'','BOX - activate SAI if invoker is SRL McLaren'),
(22,3,100100,0,0,1,0,62021,0,0,0,0,0,'','BOX - activate SAI if invoker is SRL McLaren');

DELETE FROM `achievement_criteria_data` WHERE `type`= 5 AND `criteria_id` IN (11128);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`) VALUES
(11128,5,62014,0);


-- The Turkey Chair SAI
SET @ENTRY := 34812;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,2,31,0,100,0,66250,0,0,0,86,66373,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,86,61925,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,3,0,31,0,100,0,61784,0,0,0,75,61807,0,0,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");

-- The Stuffing Chair SAI
SET @ENTRY := 34819;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,2,31,0,100,0,66259,0,0,0,86,66375,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,86,61925,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,3,0,31,0,100,0,61788,0,0,0,75,61806,0,0,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");

-- The Cranberry Chair SAI
SET @ENTRY := 34823;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,2,31,0,100,0,66261,0,0,0,86,66372,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,86,61925,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,3,0,31,0,100,0,61785,0,0,0,75,61804,0,0,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");

-- The Pie Chair SAI
SET @ENTRY := 34822;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,2,31,0,100,0,66260,0,0,0,86,66374,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,86,61925,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,3,0,31,0,100,0,61787,0,0,0,75,61805,0,0,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");

-- The Sweet Potato Chair SAI
SET @ENTRY := 34824;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,1,31,0,100,0,66262,0,0,0,86,66376,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,1,0,61,0,100,0,0,0,0,0,86,61925,2,23,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)"),
(@ENTRY,0,2,0,31,0,100,0,61786,0,0,0,75,61808,0,0,0,0,0,23,0,0,0,0,0,0,0,"Npc - Event - Action (phase) (dungeon difficulty)");
