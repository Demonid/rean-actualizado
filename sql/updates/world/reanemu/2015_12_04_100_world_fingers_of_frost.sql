-- Remove Fingers of Frost Aura
DELETE FROM `spell_linked_spell`  WHERE `spell_trigger` IN ('-44544');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES
('-44544','-74396','Fingers of Frost - Remove Aura');
