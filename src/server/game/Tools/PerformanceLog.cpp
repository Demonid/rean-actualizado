/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2013 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PerformanceLog.h"
#include "Timer.h"
#include "Config.h"

/* SystemData
Name: PerformanceLog
%Complete: 100
Autor: base by Angarius, improvements editing rewriting by Eilo
*/

void PerformanceLog::Initialize()
{
    m_worldSum = m_worldCount = m_worldMax = 0;
}

void PerformanceLog::Update(uint32 diff, uint32 realmId, uint32 uptime, uint32 sampleTime)
{
    if (diff > m_worldMax)
        m_worldMax = diff;

    ++m_worldCount;
    m_worldSum += diff;

    if (m_worldSum >= sampleTime)
    {
        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_PERFORMANCELOG);
        stmt->setUInt32(0, realmId);
        stmt->setUInt32(1, time(NULL));
        stmt->setUInt32(2, m_worldSum / m_worldCount);
        stmt->setUInt32(3, m_worldMax);
        stmt->setUInt32(4, sWorld->GetPlayerCount());
        stmt->setUInt32(5, uptime);

        LoginDatabase.Execute(stmt);

        m_worldMax = m_worldSum = m_worldCount = 0;
    }
}
