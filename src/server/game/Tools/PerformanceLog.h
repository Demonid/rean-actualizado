/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2013 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PERFORMANCE_LOG_H_
#define _PERFORMANCE_LOG_H_

#include "Common.h"
#include "ObjectMgr.h"
#include "Timer.h"
#include "World.h"
#include "DatabaseEnv.h"

class PerformanceLog
{
    public:
        static PerformanceLog* instance()
        {
            static PerformanceLog instance;
            return &instance;
        }
        // General
        void Initialize();
        void Update(uint32 diff, uint32 realmID, uint32 uptime, uint32 sampleTime);

    private:
        PerformanceLog() { };
        ~PerformanceLog() { };

        // World update diff
        uint32 m_worldCount;
        uint32 m_worldSum;
        uint32 m_worldMax;
};

#define sPerfLog PerformanceLog::instance()

#endif
