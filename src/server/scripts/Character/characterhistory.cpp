/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2012 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* SystemData
Name: CharacterHistory
%Complete: 100
Autor: Codels & Eilo
*/

#include "Config.h"
#include "WorldSession.h"
#include "Object.h"
#include "ScriptMgr.h"
#include "Player.h"

enum CharacterHistoryType
{
    CHARACTER_HISTORY_UNKNOWN = 0,
    CHARACTER_HISTORY_LOGIN   = 1,
    CHARACTER_HISTORY_LOGOUT  = 2,
    CHARACTER_HISTORY_DELETE  = 3,
    CHARACTER_HISTORY_CREATE  = 4
};

bool CharacterHistoryEnable   = false;
bool CharacterHistoryLogin    = false;
bool CharacterHistoryLogout   = false;
bool CharacterHistoryCreate   = false;
bool CharacterHistoryDelete   = false;

class CharacterHistory_WorldScript : public WorldScript
{
    public:
        CharacterHistory_WorldScript() : WorldScript("CharacterHistory_WorldScript") { }

    // Called after the world configuration is (re)loaded.
    void OnConfigLoad(bool /*reload*/)
    {
        CharacterHistoryEnable = sConfigMgr->GetBoolDefault("CharacterHistory.Enable", false);

        if (!CharacterHistoryEnable)
            return;

        CharacterHistoryLogin  = sConfigMgr->GetBoolDefault("CharacterHistory.Login", false);
        CharacterHistoryLogout = sConfigMgr->GetBoolDefault("CharacterHistory.Logout", false);
        CharacterHistoryCreate = sConfigMgr->GetBoolDefault("CharacterHistory.Create", false);
        CharacterHistoryDelete = sConfigMgr->GetBoolDefault("CharacterHistory.Delete", false);
    }
};

class CharacterHistory_PlayerScript : public PlayerScript
{
    public:
        CharacterHistory_PlayerScript() : PlayerScript("CharacterHistory_PlayerScript") { }

    // Called when a player is created.
    void OnCreate(Player* player)
    {
        if (!CharacterHistoryEnable || !CharacterHistoryCreate)
            return;

        WorldSession* session = player->GetSession();
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_HISTORY);
        stmt->setUInt32(0, session->GetAccountId());
        stmt->setUInt32(1, realmID);
        stmt->setString(2, session->GetRemoteAddress().c_str());
        stmt->setUInt32(3, CHARACTER_HISTORY_CREATE);
        stmt->setUInt32(4, player->GetGUIDLow());
        stmt->setString(5, player->GetName().c_str());

        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);
    }

    // Called when a player logs in.
    void OnLogin(Player* player, bool /*firstLogin*/)
    {
        if (!CharacterHistoryEnable || !CharacterHistoryLogin)
            return;

        WorldSession* session = player->GetSession();
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_HISTORY);
        stmt->setUInt32(0, session->GetAccountId());
        stmt->setUInt32(1, realmID);
        stmt->setString(2, session->GetRemoteAddress().c_str());
        stmt->setUInt32(3, CHARACTER_HISTORY_LOGIN);
        stmt->setUInt32(4, player->GetGUIDLow());
        stmt->setString(5, player->GetName().c_str());

        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);
    }

    // Called when a player logs out.
    void OnLogout(Player* player)
    {
        if (!CharacterHistoryEnable || !CharacterHistoryLogout)
            return;

        WorldSession* session = player->GetSession();
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_HISTORY);
        stmt->setUInt32(0, session->GetAccountId());
        stmt->setUInt32(1, realmID);
        stmt->setString(2, session->GetRemoteAddress().c_str());
        stmt->setUInt32(3, CHARACTER_HISTORY_LOGOUT);
        stmt->setUInt32(4, player->GetGUIDLow());
        stmt->setString(5, player->GetName().c_str());

        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);
    }

    // Called when a player is deleted.
    void OnDelete(ObjectGuid guid, uint32 /*accountId*/, const char* name, WorldSession* session)
    {
        if (!CharacterHistoryEnable || !CharacterHistoryDelete)
            return;

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_HISTORY);
        stmt->setUInt32(0, session->GetAccountId());
        stmt->setUInt32(1, realmID);
        stmt->setString(2, session->GetRemoteAddress().c_str());
        stmt->setUInt32(3, CHARACTER_HISTORY_DELETE);
        stmt->setUInt32(4, guid.GetCounter());
        stmt->setString(5, name);

        CharacterDatabase.Execute(stmt);
    }
};

void AddSC_characterhistory()
{
    new CharacterHistory_WorldScript();
    new CharacterHistory_PlayerScript();
}
