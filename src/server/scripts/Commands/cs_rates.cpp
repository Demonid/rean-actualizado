/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2013 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Player.h"

#pragma warning (disable : 4700)

class rates_commandscript : public CommandScript
{
public:
    rates_commandscript() : CommandScript("rates_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> ratesCommandTable =
        {
            { "kill",           rbac::RBAC_PERM_COMMAND_XPRATE_KILL,            false, &HandleXpKill,                      "" },
            { "quest",          rbac::RBAC_PERM_COMMAND_XPRATE_QUEST,           false, &HandleXpQuest,                     "" },
            { "explore",        rbac::RBAC_PERM_COMMAND_XPRATE_EXPLORE,         false, &HandleXpExplore,                   "" },
            { "defaults",       rbac::RBAC_PERM_COMMAND_XPRATE_DEFAULTS,        false, &HandleXpDefaultRates,              "" },
            { "",               rbac::RBAC_PERM_COMMAND_XPRATE,                 false, &HandleXpGlobalRates,               "" },
        };


        static std::vector<ChatCommand> commandTable =
        {
            { "xprate",         rbac::RBAC_PERM_COMMAND_XPRATE,                 false,  NULL,                               "", ratesCommandTable },
        };
        return commandTable;
    }

    static bool HandleXpGlobalRates(ChatHandler* handler, const char* args)
    {
        // Para la experiencia global hace falta tomar en cuenta las configs que hacen la exp como tal
        // y alterar en medio de los que rates weekend tambien, por ello hay que tomar en cuenta cada
        // parametro y estos son: RATE_XP_KILL, RATE_XP_QUEST, RATE_XP_EXPLORE
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
            return false;

        float xpKill = sWorld->getRate(RATE_XP_KILL);
        float xpQuest = sWorld->getRate(RATE_XP_QUEST);
        float xpExplore = sWorld->getRate(RATE_XP_EXPLORE);

        if (!*args)
        {
            handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
            handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
            handler->PSendSysMessage(LANG_XPRATE_KILL, player->GetXpKillRate());
            handler->PSendSysMessage(LANG_XPRATE_QUEST, player->GetXpQuestRate());
            handler->PSendSysMessage(LANG_XPRATE_EXPLORE, player->GetXpExploreRate());
            handler->PSendSysMessage(LANG_XPRATE_WOWREAN1);
            handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
            return true;
        }

        float xp_rate = atof((char*)args);
        if (xp_rate <= 0.0f || xp_rate > xpKill || xp_rate > xpQuest || xp_rate > xpExplore)
        {
            handler->SendSysMessage(LANG_BAD_VALUE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Seteando rates globales
        player->SetXpGlobalRates(xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
        handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
        handler->PSendSysMessage(LANG_XPRATE_KILL, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_QUEST, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_EXPLORE, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_WOWREAN2);
        handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
        return true;
    }

    static bool HandleXpDefaultRates(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
            return false;

        // Regresando la exp a los rates globales del server
        player->SetXpDefaultRates();
        handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
        handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
        handler->PSendSysMessage(LANG_XPRATE_KILL, sWorld->getRate(RATE_XP_KILL));
        handler->PSendSysMessage(LANG_XPRATE_QUEST, sWorld->getRate(RATE_XP_QUEST));
        handler->PSendSysMessage(LANG_XPRATE_EXPLORE, sWorld->getRate(RATE_XP_EXPLORE));
        handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
        return true;
    }

    static bool HandleXpKill(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
            return false;

        float xp = sWorld->getRate(RATE_XP_KILL);

        if (!*args)
            return false;

        float xp_rate = atof((char*)args);
        if (xp_rate <= 0.0f || xp_rate > xp)
        {
            handler->SendSysMessage(LANG_BAD_VALUE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Seteando rate de kill
        player->SetXpKillRate(xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
        handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
        handler->PSendSysMessage(LANG_XPRATE_KILL, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_QUEST, player->GetXpQuestRate());
        handler->PSendSysMessage(LANG_XPRATE_EXPLORE, player->GetXpExploreRate());
        handler->PSendSysMessage(LANG_XPRATE_WOWREAN3);
        handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
        return true;
    }
    
    static bool HandleXpQuest(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
            return false;

        float xp = sWorld->getRate(RATE_XP_QUEST);

        if (!*args)
            return false;

        float xp_rate = atof((char*)args);
        if (xp_rate <= 0.0f || xp_rate > xp)
        {
            handler->SendSysMessage(LANG_BAD_VALUE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Seteando rate de quest
        player->SetXpQuestRate(xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
        handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
        handler->PSendSysMessage(LANG_XPRATE_KILL, player->GetXpKillRate());
        handler->PSendSysMessage(LANG_XPRATE_QUEST, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_EXPLORE, player->GetXpExploreRate());
        handler->PSendSysMessage(LANG_XPRATE_WOWREAN3);
        handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
        return true;
    }
    
    static bool HandleXpExplore(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
            return false;

        float xp = sWorld->getRate(RATE_XP_EXPLORE);

        if (!*args)
            return false;

        float xp_rate = atof((char*)args);
        if (xp_rate <= 0.0f || xp_rate > xp)
        {
            handler->SendSysMessage(LANG_BAD_VALUE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Seteando rate de explore
        player->SetXpExploreRate(xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_UPPER_BAR);
        handler->PSendSysMessage(LANG_XPRATE_INITIAL_TEXT, player->GetName().c_str());
        handler->PSendSysMessage(LANG_XPRATE_KILL, player->GetXpKillRate());
        handler->PSendSysMessage(LANG_XPRATE_QUEST, player->GetXpQuestRate());
        handler->PSendSysMessage(LANG_XPRATE_EXPLORE, xp_rate);
        handler->PSendSysMessage(LANG_XPRATE_WOWREAN3);
        handler->PSendSysMessage(LANG_XPRATE_LOWER_BAR);
        return true;
    }
};

void AddSC_rates_commandscript()
{
    new rates_commandscript();
}
