/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ObjectMgr.h"
#include "GroupMgr.h"
#include "Group.h"
#include "CombatAI.h"
#include "Player.h"

/*######
## go_warkarts_gate
######*/

class go_warkarts_gate : public GameObjectScript
{
public:
    go_warkarts_gate() : GameObjectScript("go_warkarts_gate")
    {
        _eventGroup = NULL;
        _groupGUID.Clear();
    }

    void OnGameObjectStateChanged(GameObject* go, uint32 state)
    {
        std::list<Player*> plrList;
        go->GetPlayerListInGrid(plrList, 950.0f);

        if (!state)
        {
            for (std::list<Player*>::const_iterator itr = plrList.begin(); itr != plrList.end(); ++itr)
            {
                if (Player* player = (*itr))
                {
                    if (player->GetGroup() && _groupGUID.IsEmpty())
                        _groupGUID = player->GetGroup()->GetGUID();

                    if (!_groupGUID.IsEmpty())
                        _eventGroup = sGroupMgr->GetGroupByGUID(_groupGUID.GetCounter());

                    if (_eventGroup)
                    {
                        uint8 groupid = _eventGroup->GetMemberGroup(player->GetGUID());
                        if (groupid == 0 || groupid == 1)
                        {
                            player->UpdatePvP(true);
                            _eventGroup->RemoveMember(player->GetGUID());
                        }
                    }
                }
            }

            _eventGroup = NULL;
        }
        else
        {
            if (!_groupGUID.IsEmpty())
                _eventGroup = sGroupMgr->GetGroupByGUID(_groupGUID.GetCounter());

            if (_eventGroup)
            {
                for (std::list<Player*>::const_iterator itr = plrList.begin(); itr != plrList.end(); ++itr)
                {
                    if (Player* player = (*itr))
                        if (!player->IsGameMaster())
                        {
                            if (!player->GetGroup())
                            {
                                _eventGroup->AddMember(player->ToPlayer());
                                player->UpdatePvP(false);
                            }
                        }
                }
            }

            _eventGroup = NULL;
            _groupGUID.Clear();
        }
    }
private:
    Group* _eventGroup;
    ObjectGuid _groupGUID;
};

/*######
## warkart_vehicle AI
######*/


enum miscData
{
    // Spells
    SPELL_COSMETIC_EXPLOTION    = 60080,
    // gameobjects
    GO_WARKARTS_DOOR            = 700011
};

class warkart_vehicle : public CreatureScript
{
public:
    warkart_vehicle() : CreatureScript("warkart_vehicle") { }

    struct warkart_vehicleAI : public VehicleAI
    {
        warkart_vehicleAI(Creature* creature) : VehicleAI(creature), _eventDoor(ObjectGuid::Empty), _yellPlayer("Soy noob!!"){ }

        void PassengerBoarded(Unit* player, int8 /*seatId*/, bool apply)
        {
            if (apply)
            {
                if (GameObject* door = me->FindNearestGameObject(GO_WARKARTS_DOOR, 120.0f))
                    _eventDoor = door->GetGUID();
            }
            else if (_eventDoor)
            {
                if (GameObject* Door = sObjectAccessor->GetGameObject(*me, _eventDoor))
                    if (Door->GetGoState() == GO_STATE_ACTIVE)
                    {
                        player->CastSpell(me, SPELL_COSMETIC_EXPLOTION);
                        me->DespawnOrUnsummon(4 * IN_MILLISECONDS);
                        player->ToPlayer()->Yell(_yellPlayer, LANG_UNIVERSAL);
                        player->Kill(player, false);
                    }
            }
        }

        void UpdateAI(uint32 diff) override
        {

        }
    private:
        ObjectGuid _eventDoor;
        // Player Text
        std::string _yellPlayer;
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new warkart_vehicleAI(creature);
    }
};

void AddSC_warkarts_scripts()
{
    new go_warkarts_gate;
    new warkart_vehicle();
}
