/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2012 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* SystemData
Name: RaidInfo
%Complete: 100
Autor: Some various
*/

#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Map.h"
#include "Player.h"
#include "Pet.h"

class duel_reset_cds : public PlayerScript
{
    public:
        duel_reset_cds() : PlayerScript("duel_reset_cds") {}

        void OnDuelStart(Player* player1, Player* player2)
        {
            if (player1->GetZoneId() == 14 || player1->GetZoneId() == 12)
            {
                // Filling up players HP/Power bars
                player1->ResetAllPowers();
                player2->ResetAllPowers();

                // Filling up Pet HP/Power if any (Thanks to Mortos)
                if (Pet* p1pet = player1->GetPet())
                {
                    p1pet->SetHealth(p1pet->GetMaxHealth());
                    if (Powers power = p1pet->getPowerType())
                        p1pet->SetPower(power, p1pet->GetMaxPower(power));
                }

                if (Pet* p2pet = player2->GetPet())
                {
                    p2pet->SetHealth(p2pet->GetMaxHealth());
                    if (Powers power = p2pet->getPowerType())
                        p2pet->SetPower(power, p2pet->GetMaxPower(power));
                }
            }
        }

        void OnDuelEnd(Player *winner, Player *loser, DuelCompleteType type)
        {
            if(winner->GetZoneId() == 14 || winner->GetZoneId() == 12)
            {
                if (type == DUEL_WON)
                {
                    // Clearing CDs from players
                    // and their pets if any (Thanks to Mortos)
                    winner->RemoveArenaSpellCooldowns(true);
                    loser->RemoveArenaSpellCooldowns(true);

                    // Removing Debuffs
                    winner->RemoveAura(41425); // Remove Hypothermia Debuff
                    loser->RemoveAura(41425);
                    winner->RemoveAura(25771); // Remove Forbearance Debuff
                    loser->RemoveAura(25771);
                    winner->RemoveAura(57724); // Remove Sated Debuff
                    loser->RemoveAura(57724);
                    winner->RemoveAura(57723); // Remove Exhaustion Debuff
                    loser->RemoveAura(57723);
                    winner->RemoveAura(31850); // Remove Ardent Defender Debuff
                    loser->RemoveAura(31850);
                    winner->RemoveAura(11196); // Remove Recently Bandaged Debuff
                    loser->RemoveAura(11196);
                    winner->ClearDiminishings();
                    loser->ClearDiminishings(); // Reset Diminishing Returns

                    // Filling up players HP/Power bars
                    // este m�todo hace lo mismo que antes y queda la script m�s prolija y limpia
                    winner->ResetAllPowers();
                    loser->ResetAllPowers();

                    // Filling up Pet HP/Power if any (Thanks to Mortos)
                    if (Pet* p1pet = winner->GetPet())
                    {
                        p1pet->SetHealth(p1pet->GetMaxHealth());
                        if (Powers power = p1pet->getPowerType())
                            p1pet->SetPower(power, p1pet->GetMaxPower(power));
                    }

                    if (Pet* p2pet = loser->GetPet())
                    {
                        p2pet->SetHealth(p2pet->GetMaxHealth());
                        if (Powers power = p2pet->getPowerType())
                            p2pet->SetPower(power, p2pet->GetMaxPower(power));
                    }

                }
                winner->HandleEmoteCommand(EMOTE_ONESHOT_CHEER);
            }
        }
};

void AddSC_DuelReset()
{
    new duel_reset_cds();
}
