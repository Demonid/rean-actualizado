/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2012 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: Arena Espectador
%Complete: 100
Autor: Eilo
Comment: El npc asigando con esta script tiene la habilidad de hacer que el
         jugador que seleccione la debida opcion pueda entrar en cierta arena
         con un estado de observador y no se le permitira intervenir en la
         partida.

         Tiene tres partes, en el hello, se listaran los matches por zonas y
         entregara un total de partidas por ladder, en el select lista los
         nombres y ratings de los matches en ese momento validando ciertas 
         cosas, y al seleccionar recorrera los offsets para que entre en la
         partida adecuada, ya que esta dividido por zonas. Y por ultimo al
         ingresar un nombre en el codebox, te teleporta a la ubicacion de ese
         pj que obviamente tiene q estar en alguna arena.

######
## Arena Espectador
######*/

#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "ObjectAccessor.h"
#include "ArenaTeamMgr.h"
#include "ArenaTeam.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptedCreature.h"
#include "ScriptMgr.h"

enum EspectadorData
{
    GOSSIP_OFFSET = GOSSIP_ACTION_INFO_DEF + 5,
};

class npc_arena_espectador : public CreatureScript
{
public:
    npc_arena_espectador() : CreatureScript("npc_arena_espectador") {}
    
    // Saludando, apenas le clickeamos el npc nos entregara la lista completa de matches en ese momento
    bool OnGossipHello(Player* player, Creature* creature)
    {
        // Variables usadas aqui,
        // el arenaSet y el arenasQty son holders para el tipo de arena y del Quantity o cantidad de gente
        BattlegroundContainer arenasSet = sBattlegroundMgr->GetAllBattlegroundsWithAllTypeIds();
        uint32 arenasQty[3] = {0, 0, 0};
        // Declarando las strings para mostrar texto en el menu principal
        std::stringstream gossip2;
        std::stringstream gossip3;
        std::stringstream gossip5;

        // Revisamos las arenas en un solo for, los parentesis es solo para consistencia (por sea caso xd)
        for (BattlegroundContainer::const_iterator itr = arenasSet.begin(); itr != arenasSet.end(); ++itr)
        {
            if (Battleground* bg = itr->second)
            {
                if (bg->GetStatus() == STATUS_IN_PROGRESS && bg->isRated()) // Solo si ya esta iniciada y estan peleando y es rankeada
                {
                    switch (bg->GetArenaType())
                    {
                    case ARENA_TYPE_2v2:
                        arenasQty[0]++;
                        break;
                    case ARENA_TYPE_3v3:
                        arenasQty[1]++;
                        break;
                    case ARENA_TYPE_5v5:
                        arenasQty[2]++;
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        // Armando los textos
        gossip2 << "Observar una arena 2v2 . (" << arenasQty[0] << " partida(s) ahora mismo). WowRean";
        gossip3 << "Observar una arena 3v3 . (" << arenasQty[1] << " partida(s) ahora mismo). WowRean";
        gossip5 << "Observar una arena 5v5 . (" << arenasQty[2] << " partida(s) ahora mismo). WowRean";
        // Asignando los gossips
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossip2.str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossip3.str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossip5.str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
        // El ultimo gossip para players
        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Observar a un Jugador. WowRean", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4, "", 0, true);
        // Enviando al player el menu
        player->PlayerTalkClass->SendGossipMenu(player->GetGossipTextId(creature), creature->GetGUID());
        return true;
    }

    // Aqui cuando usamos alguna de las opciones, o le damos click a alguna del menu se aparecera lo siguiente
    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
    {
        // Nueva pagina
        player->PlayerTalkClass->ClearMenus();
        // Para seleccionar a manera de cases
        uint8 mode = ARENA_TYPE_2v2;
        if (action == (GOSSIP_ACTION_INFO_DEF + 3))
            mode = ARENA_TYPE_3v3;
        if (action == (GOSSIP_ACTION_INFO_DEF + 5))
            mode = ARENA_TYPE_5v5;

        // Si hemos seleccionado una opcion valida (el offset esta en 10, solo desplegara ese numero de matches)
        if (action <= GOSSIP_OFFSET)
        {
            // Holders para esta parte, serviran para ayudar a listar dentro de cada bracket los equipos y ratings
            BattlegroundContainer arenasSet = sBattlegroundMgr->GetAllBattlegroundsWithAllTypeIds();
            bool bracketMatchs = false;

            // Revisamos las arenas en un solo for
            for (BattlegroundContainer::const_iterator itr = arenasSet.begin(); itr != arenasSet.end(); ++itr)
            {
                if (Battleground* bg = itr->second)
                {
                    // Solo seleccionar la que coincide con el ladder
                    if (bg->GetArenaType() == mode)
                    {
                        if (bg->GetStatus() == STATUS_IN_PROGRESS && bg->isRated())// Solo si ya esta iniciada y estan peleando y es rankeada
                        {
                            ArenaTeam *firstTeam = sArenaTeamMgr->GetArenaTeamById(bg->GetArenaTeamIdByIndex(0));
                            ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(bg->GetArenaTeamIdByIndex(1));

                            if (firstTeam && secondTeam)
                            {
                                // String para imprimir los nombres de las clases
                                std::stringstream teamOne;
                                std::stringstream teamTwo;
                                // Esta parte es para saber las clases que conforman de los del equipo
                                // Primero me armo una lista para buscar dentro de los mapas de arenas
                                for (Battleground::BattlegroundPlayerMap::const_iterator itr_class = bg->GetPlayers().begin(); itr_class != bg->GetPlayers().end(); ++itr_class)
                                {
                                    if (Player* player = ObjectAccessor::FindPlayer(itr_class->first)) // Player :)
                                    {
                                        // para imprimir el clase y spec
                                        uint8 spec1 = 0;
                                        uint8 spec2 = 0;
                                        uint8 spec3 = 0;
                                        uint8 clase = player->getClass();
                                        uint32 specfor = player->GetActiveSpec();
                                        std::stringstream class_spec;

                                        // talentos
                                        uint32 const* talentTabIds = GetTalentTabPages(clase);
                                        for (uint8 i = 0; i < MAX_TALENT_TABS; ++i)
                                        {
                                            uint32 talentTabId = talentTabIds[i];

                                            for (uint32 talentId = 0; talentId < sTalentStore.GetNumRows(); ++talentId)
                                            {
                                                TalentEntry const* talentInfo = sTalentStore.LookupEntry(talentId);
                                                if (!talentInfo)
                                                    continue;

                                                if (talentInfo->TalentTab != talentTabId)
                                                    continue;

                                                int8 curtalent_maxrank = -1;
                                                for (int8 rank = MAX_TALENT_RANK - 1; rank >= 0; --rank)
                                                {
                                                    if (talentInfo->RankID[rank] && player->HasTalent(talentInfo->RankID[rank], specfor))
                                                    {
                                                        curtalent_maxrank = rank;
                                                        break;
                                                    }
                                                }

                                                if (curtalent_maxrank < 0)
                                                    continue;

                                                switch (i)
                                                {
                                                case 0:     spec1 = spec1 + 1 + curtalent_maxrank;      break;
                                                case 1:     spec2 = spec2 + 1 + curtalent_maxrank;      break;
                                                case 2:     spec3 = spec3 + 1 + curtalent_maxrank;      break;
                                                default:   continue;   break;
                                                }
                                            }
                                        }

                                        // clase y spec
                                        switch (clase)
                                        {
                                        case CLASS_WARRIOR:
                                            class_spec << "Warro ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Prote";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Furia";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Armas";
                                            break;
                                        case CLASS_PALADIN:
                                            class_spec << "Pala ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Repre";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Prote";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Holy";
                                            break;
                                        case CLASS_HUNTER:
                                            class_spec << "Caza ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Surv";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Punteria";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Bestias";
                                            break;
                                        case CLASS_ROGUE:
                                            class_spec << "Pica ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Suti";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Combat";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Asesinato";
                                            break;
                                        case CLASS_PRIEST:
                                            class_spec << "Sacer ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Shadow";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Holy";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Disci";
                                            break;
                                        case CLASS_DEATH_KNIGHT:
                                            class_spec << "Dk ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Unholy";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Frost";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Blood";
                                            break;
                                        case CLASS_SHAMAN:
                                            class_spec << "Shami ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Resto";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Mejora";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Elemental";
                                            break;
                                        case CLASS_MAGE:
                                            class_spec << "Mago ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Frost";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Fuego";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Arcano";
                                            break;
                                        case CLASS_WARLOCK:
                                            class_spec << "Brujo ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Destru";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Demo";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Afli";
                                            break;
                                        case CLASS_DRUID:
                                            class_spec << "Dudu ";
                                            if (spec3 > spec2 && spec3 > spec1)
                                                class_spec << "Resto";
                                            else if (spec2 > spec3 && spec2 > spec1)
                                                class_spec << "Feral";
                                            else if (spec1 > spec2 && spec1 > spec3)
                                                class_spec << "Balance";
                                            break;
                                        }

                                        if (firstTeam->IsMember(player->GetGUID())) // Si pertenece al equipo1
                                            teamOne << class_spec.str() << " ";
                                        else if (secondTeam->IsMember(player->GetGUID())) // Si pertenece al equipo2
                                            teamTwo << class_spec.str() << " ";
                                    }
                                }
                                // Cerrando la string para imprimir
                                std::stringstream gossipItem;
                                gossipItem << firstTeam->GetName() << "   VS   " << secondTeam->GetName() << "\n";
                                gossipItem << "(" << firstTeam->GetRating() << ") [ " << teamOne.str() << "] VS ";
                                gossipItem << "(" << secondTeam->GetRating() << ") [ " << teamTwo.str() << "]";
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossipItem.str(), GOSSIP_SENDER_MAIN + 1, itr->first + GOSSIP_OFFSET);
                                bracketMatchs = true;
                            }
                        }
                    }
                }
            }
            // Si no se ha encontrado coincidencias de matches
            if (!bracketMatchs)
            {
                std::stringstream errMsg;
                errMsg << "Lo sentimos " << player->GetName() << ", no se encontraron partidas disponibles. WowRean";
                creature->Whisper(errMsg.str().c_str(), LANG_UNIVERSAL, player);
                player->PlayerTalkClass->ClearMenus();
                player->CLOSE_GOSSIP_MENU();
                return false;
            }
            // Al final cuando ya hemos acabado de listar a todos los matchs en los brackets
            player->PlayerTalkClass->SendGossipMenu(player->GetGossipTextId(creature), creature->GetGUID());
            
        }
        else
        {
            // Si se usa una de las opciones y se ha ido una arena ya, metodo de actualizacion bajo por peticion
            uint32 arenaId = action - GOSSIP_OFFSET;
            // Holder
            BattlegroundContainer arenasSet = sBattlegroundMgr->GetAllBattlegroundsWithAllTypeIds();

            // Funciones para teleportar al player
            if (arenasSet[arenaId] != NULL)
            {
                Battleground* arenaChosen = arenasSet[arenaId];
                if (arenaChosen->isRated())
                {
                    uint32 slot = arenaChosen->GetArenaType() - 2;
                    if (arenaChosen->GetArenaType() > 3)
                        slot = 2;

                    ArenaTeam *firstTeam = sArenaTeamMgr->GetArenaTeamById(arenaChosen->GetArenaTeamIdByIndex(0));
                    ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(arenaChosen->GetArenaTeamIdByIndex(1));
                    if (firstTeam && secondTeam)
                    {
                        std::stringstream infoMsg;
                        infoMsg << "Entraste a una arena puntuada: \n";
                        infoMsg << "Equipos: \'" << firstTeam->GetName().c_str() << "\'  -  \'" << secondTeam->GetName().c_str() << "\' \n";
                        infoMsg << "Rating: " << firstTeam->GetRating() << "  -  " << secondTeam->GetRating() << "\n";
                        infoMsg << "MMR: " << arenaChosen->GetArenaMatchmakerRating(0) << "  -  " << arenaChosen->GetArenaMatchmakerRating(1) << "\n";
                        infoMsg << "WowRean";
                        creature->Whisper(infoMsg.str().c_str(), LANG_UNIVERSAL, player);
                    }
                }

                player->SetBattlegroundId(arenaChosen->GetInstanceID(), arenaChosen->GetTypeID());
                player->SetBattlegroundEntryPoint();
                float x, y, z;
                switch (arenaChosen->GetMapId())
                {
                    case 617: x = 1299.046f;    y = 784.825f;     z = 9.338f;     break;
                    case 618: x = 763.5f;       y = -284;         z = 28.276f;    break;
                    case 572: x = 1285.810547f; y = 1667.896851f; z = 39.957642f; break;
                    case 562: x = 6238.930176f; y = 262.963470f;  z = 0.889519f;  break;
                    case 559: x = 4055.504395f; y = 2919.660645f; z = 13.611241f; break;
                }

                player->SaveRecallPosition();
                player->SetSpectate(true);
                player->TeleportTo(arenaChosen->GetMapId(), x, y, z, player->GetOrientation());
                player->GetBattleground()->AddSpectator(player->GetGUID());
            }
        }
        return true;
    }

    // Este es el select especial del player, este toca mirar denuevo a ver que pasa
    bool OnGossipSelectCode(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction, const char* code)
    {
        // Limpia la pantalla de menus
        player->PlayerTalkClass->ClearMenus();
        // para cerrar el primer menu
        player->CLOSE_GOSSIP_MENU();
        // para la ventanita overlapping
        if (uiSender == GOSSIP_SENDER_MAIN)
        {
            switch (uiAction)
            {
                // Buscar por nombre de jugador
                case GOSSIP_ACTION_INFO_DEF + 4:
                {
                    const char* plrName = code;
                    if (Player* target = sObjectAccessor->FindPlayerByName(plrName))
                    {
                        if (!target->IsInWorld())
                        {
                            creature->Whisper("No puedes observar a un pj offline. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (target == player || target->GetGUID() == player->GetGUID())
                        {
                            creature->Whisper("No puedes ser espectador de ti mismo. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        if (player->IsInCombat())
                        {
                            creature->Whisper("No puedes ser espectador mientras estas en combate. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (!target->InArena())
                        {
                            creature->Whisper("El jugador no esta en arenas. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (target->IsGameMaster())
                        {
                            creature->Whisper("No se permite observar a un MJ. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (player->IsMounted())
                        {
                            creature->Whisper("No puedes espectar si estas montado. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (player->GetPet())
                        {
                            creature->Whisper("No puedes espectar con mascotas. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (player->GetMap()->IsBattlegroundOrArena() && !player->isSpectator())
                        {
                            creature->Whisper("Ya estas en una BG o Arena. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        Map* cMap = target->GetMap();
                        if (!cMap->IsBattleArena())
                        {
                            creature->Whisper("El jugador no esta en ninguna arena. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }
                        if (player->GetMap()->IsBattleground())
                        {
                            creature->Whisper("No puedes hacer eso mientras estas en una bg. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        if (target->isSpectator())
                        {
                            creature->Whisper("No puedes ser espectador de un espectador. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        if (player->getSpectateFrom())
                        {
                            creature->Whisper("No puedes ser espectador si aun estas espiando a alguien. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        // holder de la bg del player seleccionado
                        Battleground *bg = target->GetBattleground();

                        if (bg->GetStatus() != STATUS_IN_PROGRESS)
                        {
                            creature->Whisper("No puedes ser espectador en una arena que esta en etapa de preparacion o finalizacion. WowRean", LANG_UNIVERSAL, player);
                            return true;
                        }

                        if (bg->isRated())
                        {
                            uint32 slot = bg->GetArenaType() - 2;
                            if (bg->GetArenaType() > 3)
                                slot = 2;

                            ArenaTeam *firstTeam = sArenaTeamMgr->GetArenaTeamById(bg->GetArenaTeamIdByIndex(0));
                            ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(bg->GetArenaTeamIdByIndex(1));
                            if (firstTeam && secondTeam)
                            {
                                std::stringstream infoMsg;
                                infoMsg << "Entraste a una arena puntuada: \n";
                                infoMsg << "Equipos: \'" << firstTeam->GetName().c_str() << "\'  -  \'" << secondTeam->GetName().c_str() << "\' \n";
                                infoMsg << "Rating: " << firstTeam->GetRating() << "  -  " << secondTeam->GetRating() << "\n";
                                infoMsg << "MMR: " << bg->GetArenaMatchmakerRating(0) << "  -  " << bg->GetArenaMatchmakerRating(1) << "\n";
                                infoMsg << "WowRean";
                                creature->Whisper(infoMsg.str().c_str(), LANG_UNIVERSAL, player);
                            }
                        }

                        // añadiendole a la bg y grabando la ubicacion actual como entrypoint para cuando salgas
                        player->SetBattlegroundId(target->GetBattleground()->GetInstanceID(), target->GetBattleground()->GetTypeID());
                        player->SetBattlegroundEntryPoint();

                        // estas dos lineas es para aparecer con la misma orientacion del target
                        float x, y, z;
                        target->GetContactPoint(player, x, y, z);

                        // guardando la posicion
                        player->SaveRecallPosition();

                        // al mapa
                        player->TeleportTo(target->GetMapId(), x, y, z, player->GetAngle(target));
                        player->SetPhaseMask(target->GetPhaseMask(), true); // el phasemask yei
                        player->SetSpectate(true); // ya arena espectador
                        player->SetViewpoint(target, true);
                        target->GetBattleground()->AddSpectator(player->GetGUID()); // para el contenedor nuevo
                    }
                    else
                    {
                        creature->Whisper("No se encuentra el jugador, recuerda escribirlo bien. WowRean", LANG_UNIVERSAL, player);
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    }
};

void AddSC_npc_arena_espectador()
{
    new npc_arena_espectador;
}
