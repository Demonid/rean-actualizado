/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2009-2012 Eilo <https://github.com/eilo>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* SystemData
Name: RaidInfo
%Complete: 100
Autor: Eilo
*/

#include "Config.h"
#include "Group.h"
#include "Guild.h"
#include "GuildMgr.h"
#include "ScriptMgr.h"
#include "Chat.h"
#include "GroupMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Map.h"
#include "Player.h"
#include "MapManager.h"
#include "MapInstanced.h"
#include "InstanceSaveMgr.h"
#include "InstanceScript.h"

#define DATA_SEPARATOR ":"
#define PEOPLE_SEPARATOR " "

enum RaidInfoBossId
{
    // ----------------   TBC  ------------------ //
    BLACK_TEMPLE_NAJENTUS               = 22887,
    BLACK_TEMPLE_SUPREMUS               = 22898,
    BLACK_TEMPLE_AKAMA                  = 22841,
    BLACK_TEMPLE_TERON                  = 22871,
    BLACK_TEMPLE_GURTOGG                = 22948,
    // relicario son 3 pero segun vi en los videos tambien
    // muere al final el controller que es el 22856
    BLACK_TEMPLE_RELIQUARY              = 22856,
    BLACK_TEMPLE_RELIQUARY_SUFFERING    = 23418,
    BLACK_TEMPLE_RELIQUARY_DESIRE       = 23419,
    BLACK_TEMPLE_RELIQUARY_ANGER        = 23420,
    BLACK_TEMPLE_MOTHER                 = 22947,
    // consejo si no se como va el script, dejo todas las ids
    BLACK_TEMPLE_COUNCIL                = 23426,
    BLACK_TEMPLE_COUNCIL_GATHIOS        = 22949,
    BLACK_TEMPLE_COUNCIL_ZEREVOR        = 22950,
    BLACK_TEMPLE_COUNCIL_MALANDE        = 22951,
    BLACK_TEMPLE_COUNCIL_VERAS          = 22952,
    BLACK_TEMPLE_ILLIDAN                = 22917,
    HYJAL_RAGE_WINTERCHILL              = 17767,
    HYJAL_ANETHERON                     = 17808,
    HYJAL_KAZROGAL                      = 17888,
    HYJAL_AZGALOR                       = 17842,
    HYJAL_ARCHIMONDE                    = 17968,
    SSC_HYDROSS                         = 21216,
    SSC_LURKER                          = 21217,
    SSC_LEOTHERAS                       = 21215,
    SSC_KARATHRESS                      = 21214,
    SSC_MOROGRIM                        = 21213,
    SSC_LADY_VASHJ                      = 21212,
    GRUULS_LAIR_MAULGAR                 = 18831,
    GRUULS_LAIR_GRUUL                   = 19044,
    MAGTHERIDONS_LAIR_MAGTHERIDON       = 17257,
    KARAZHAN_ROKAD                      = 16181,
    KARAZHAN_SHADIKITH                  = 16180,
    KARAZHAN_HYAKISS                    = 16179,
    KARAZHAN_ATTUMEN                    = 16152,
    KARAZHAN_MOROES                     = 15687,
    KARAZHAN_MAIDEN                     = 16457,
    // aqui igual tengo dudas con la opera, me tocaria
    // como en valithria llamarla en una parte del 
    // script del encuentro al hook de aqui
    KARAZHAN_OPERA                      = 16812,
    KARAZHAN_DOROTHEE                   = 17535,
    KARAZHAN_TITO                       = 17548,
    KARAZHAN_STRAWMAN                   = 17543,
    KARAZHAN_TINHEAD                    = 17547,
    KARAZHAN_ROAR                       = 17646,
    KARAZHAN_THE_CRONE                  = 18168,
    KARAZHAN_WOLF                       = 17521,
    KARAZHAN_ROMULO                     = 17533,
    KARAZHAN_JULIANNE                   = 17534,
    KARAZHAN_CURATOR                    = 15691,
    // chess event, esto esta como gameobject y puff
    // lejos de tenerlo scriptado y tal
    // KARAZHAN_CHESS                      = 185119,
    KARAZHAN_TERESTIAN                  = 15688,
    KARAZHAN_ARAN                       = 16524,
    KARAZHAN_NETHERSPITE                = 15689,
    KARAZHAN_NIGHTBANE                  = 17225,
    KARAZHAN_MALCHEZAAR                 = 15690,
    // kalecgos y sathrovarr mueren juntos hasta donde me acuerdo
    SUNWELL_PLATEAU_KALECGOS            = 24850,
    SUNWELL_PLATEAU_SATHROVARR          = 24892,
    SUNWELL_PLATEAU_BRUTALLUS           = 24882,
    SUNWELL_PLATEAU_FELMYST             = 25038,
    // alythess y sacrolash mueren juntas creo
    SUNWELL_PLATEAU_ALYTHESS            = 25166,
    SUNWELL_PLATEAU_SACROLASH           = 25165,
    // entropius es la evolucion de muru, mismo encuentro
    // SUNWELL_PLATEAU_MURU                = 25741,
    SUNWELL_PLATEAU_ENTROPIUS           = 25840,
    SUNWELL_PLATEAU_KILJAEDEN           = 25315,
    THE_EYE_VOID_REAPER                 = 19516,
    THE_EYE_ALAR                        = 19514,
    THE_EYE_SOLARIAN                    = 18805,
    // kaelthas tiene esos guardianes, voy a poner las ids nomas
    THE_EYE_KAELTHAS                    = 19622,
    THE_EYE_THALADRED                   = 20064,
    THE_EYE_SANGUINAR                   = 20060,
    THE_EYE_CAPERNIAN                   = 20062,
    THE_EYE_TELONICUS                   = 20063,
    // ----------------   TLK  ------------------ //
    TRIAL_OF_THE_CRUSADER_ICEHOWL_10N   = 34797,
    TRIAL_OF_THE_CRUSADER_ICEHOWL_25N   = 35447,
    TRIAL_OF_THE_CRUSADER_ICEHOWL_10H   = 35448,
    TRIAL_OF_THE_CRUSADER_ICEHOWL_25H   = 35449,
    TRIAL_OF_THE_CRUSADER_JARAXXUS_10N  = 34780,
    TRIAL_OF_THE_CRUSADER_JARAXXUS_25N  = 35216,
    TRIAL_OF_THE_CRUSADER_JARAXXUS_10H  = 35268,
    TRIAL_OF_THE_CRUSADER_JARAXXUS_25H  = 35269,
    TRIAL_OF_THE_CRUSADER_FJOLA_10N     = 34497,
    TRIAL_OF_THE_CRUSADER_FJOLA_25N     = 35350,
    TRIAL_OF_THE_CRUSADER_FJOLA_10H     = 35351,
    TRIAL_OF_THE_CRUSADER_FJOLA_25H     = 35352,
    TRIAL_OF_THE_CRUSADER_EYDIS_10N     = 34496,
    TRIAL_OF_THE_CRUSADER_EYDIS_25N     = 35347,
    TRIAL_OF_THE_CRUSADER_EYDIS_10H     = 35348,
    TRIAL_OF_THE_CRUSADER_EYDIS_25H     = 35349,
    TRIAL_OF_THE_CRUSADER_ANUBARAK_10N  = 34564,
    TRIAL_OF_THE_CRUSADER_ANUBARAK_25N  = 34566,
    TRIAL_OF_THE_CRUSADER_ANUBARAK_10H  = 35615,
    TRIAL_OF_THE_CRUSADER_ANUBARAK_25H  = 35616,
    ICC_MARROWGAR_10N                   = 36612,
    ICC_MARROWGAR_25N                   = 37957,
    ICC_MARROWGAR_10H                   = 37958,
    ICC_MARROWGAR_25H                   = 37959,
    ICC_DEATHWHISPER_10N                = 36855,
    ICC_DEATHWHISPER_25N                = 38106,
    ICC_DEATHWHISPER_10H                = 38296,
    ICC_DEATHWHISPER_25H                = 38297,
    ICC_GUNSHIP_SKYBREAKER_10N          = 37540,
    ICC_GUNSHIP_SKYBREAKER_25N          = 38128,
    ICC_GUNSHIP_SKYBREAKER_10H          = 38699,
    ICC_GUNSHIP_SKYBREAKER_25H          = 38700,
    ICC_GUNSHIP_ORGRIMS_HAMMER_10N      = 37215,
    ICC_GUNSHIP_ORGRIMS_HAMMER_25N      = 38129,
    ICC_GUNSHIP_ORGRIMS_HAMMER_10H      = 38701,
    ICC_GUNSHIP_ORGRIMS_HAMMER_25H      = 38702,
    ICC_SAURFANG_10N                    = 37813,
    ICC_SAURFANG_25N                    = 38402,
    ICC_SAURFANG_10H                    = 38582,
    ICC_SAURFANG_25H                    = 38583,
    ICC_FESTERGUT_10N                   = 36626,
    ICC_FESTERGUT_25N                   = 37504,
    ICC_FESTERGUT_10H                   = 37505,
    ICC_FESTERGUT_25H                   = 37506,
    ICC_ROTFACE_10N                     = 36627,
    ICC_ROTFACE_25N                     = 38390,
    ICC_ROTFACE_10H                     = 38549,
    ICC_ROTFACE_25H                     = 38550,
    ICC_PUTRICIDE_10N                   = 36678,
    ICC_PUTRICIDE_25N                   = 38431,
    ICC_PUTRICIDE_10H                   = 38585,
    ICC_PUTRICIDE_25H                   = 38586,
    ICC_VALANAR_10N                     = 37970,
    ICC_VALANAR_25N                     = 38401,
    ICC_VALANAR_10H                     = 38784,
    ICC_VALANAR_25H                     = 38785,
    ICC_KELESETH_10N                    = 37972,
    ICC_KELESETH_25N                    = 38399,
    ICC_KELESETH_10H                    = 38769,
    ICC_KELESETH_25H                    = 38770,
    ICC_TALDARAM_10N                    = 37973,
    ICC_TALDARAM_25N                    = 38400,
    ICC_TALDARAM_10H                    = 38771,
    ICC_TALDARAM_25H                    = 38772,
    ICC_LANATHEL_10N                    = 37955,
    ICC_LANATHEL_25N                    = 38434,
    ICC_LANATHEL_10H                    = 38435,
    ICC_LANATHEL_25H                    = 38436,
    ICC_VALITHRIA_10N                   = 36789,
    ICC_VALITHRIA_25N                   = 38174,
    ICC_SINDRAGOSA_10N                  = 36853,
    ICC_SINDRAGOSA_25N                  = 38265,
    ICC_SINDRAGOSA_10H                  = 38266,
    ICC_SINDRAGOSA_25H                  = 38267,
    ICC_LICHKING_10N                    = 36597,
    ICC_LICHKING_25N                    = 39166,
    ICC_LICHKING_10H                    = 39167,
    ICC_LICHKING_25H                    = 39168,
    NAXXRAMAS_MAEXXNA_10N               = 15952,
    NAXXRAMAS_MAEXXNA_25N               = 29278,
    NAXXRAMAS_LOATHEB_10N               = 16011,
    NAXXRAMAS_LOATHEB_25N               = 29718,
    NAXXRAMAS_HORSEMEN_THANE_10N        = 16064,
    NAXXRAMAS_HORSEMEN_THANE_25N        = 30603,
    NAXXRAMAS_HORSEMEN_BLAUMEUX_10N     = 16065,
    NAXXRAMAS_HORSEMEN_BLAUMEUX_25N     = 30601,
    NAXXRAMAS_HORSEMEN_RIVENDARE_10N    = 30549,
    NAXXRAMAS_HORSEMEN_RIVENDARE_25N    = 30600,
    NAXXRAMAS_HORSEMEN_ZELIEK_10N       = 16063,
    NAXXRAMAS_HORSEMEN_ZELIEK_25N       = 30602,
    NAXXRAMAS_THADDIUS_10N              = 15928,
    NAXXRAMAS_THADDIUS_25N              = 29448,
    NAXXRAMAS_KELTHUZAD_10N             = 15990,
    NAXXRAMAS_KELTHUZAD_25N             = 30061,
    ONYXIAS_LAIR_ONYXIA_10N             = 10184,
    ONYXIAS_LAIR_ONYXIA_25N             = 36538,
    EYE_OF_ETERNITY_MALYGOS             = 28859,
    ULDUAR_LEVIATHAN_10N                = 33113,
    ULDUAR_LEVIATHAN_25N                = 34003,
    ULDUAR_IGNIS_10N                    = 33118,
    ULDUAR_IGNIS_25N                    = 33190,
    ULDUAR_RAZORSCALE_10N               = 33186,
    ULDUAR_RAZORSCALE_25N               = 33724,
    ULDUAR_XT002_10N                    = 33293,
    ULDUAR_XT002_25N                    = 33885,
    ULDUAR_BRUNDIR_10N                  = 32857,
    ULDUAR_BRUNDIR_25N                  = 33694,
    ULDUAR_MOLGEIN_10N                  = 32927,
    ULDUAR_MOLGEIN_25N                  = 33692,
    ULDUAR_STEELBREAKER_10N             = 32867,
    ULDUAR_STEELBREAKER_25N             = 33693,
    ULDUAR_KOLOGARN_10N                 = 32930,
    ULDUAR_KOLOGARN_25N                 = 33909,
    ULDUAR_AURIAYA_10N                  = 33515,
    ULDUAR_AURIAYA_25N                  = 34175,
    ULDUAR_FREYA_10N                    = 32906,
    ULDUAR_FREYA_25N                    = 33360,
    ULDUAR_HODIR_10N                    = 32845,
    ULDUAR_HODIR_25N                    = 32846,
    ULDUAR_MIMIRON                      = 33350,
    ULDUAR_THORIM_10N                   = 32865,
    ULDUAR_THORIM_25N                   = 33147,
    ULDUAR_GENERALVEZAX_10N             = 33271,
    ULDUAR_GENERALVEZAX_25N             = 33449, 
    ULDUAR_YOGGSARON_10N                = 33288,
    ULDUAR_YOGGSARON_25N                = 33955,
    ULDUAR_ALGALON_10N                  = 32871,
    ULDUAR_ALGALON_25N                  = 33070,
    VAULT_OF_ARCHAVON_ARCHAVON_10N      = 31125,
    VAULT_OF_ARCHAVON_ARCHAVON_25N      = 31722,
    VAULT_OF_ARCHAVON_EMALON_10N        = 33993,
    VAULT_OF_ARCHAVON_EMALON_25N        = 33994,
    VAULT_OF_ARCHAVON_KORALON_10N       = 35013,
    VAULT_OF_ARCHAVON_KORALON_25N       = 35360,
    VAULT_OF_ARCHAVON_TORAVON_10N       = 38433,
    VAULT_OF_ARCHAVON_TORAVON_25N       = 38462,
    OBSIDIAN_SANCTUM_SARTHARION_10N     = 28860,
    OBSIDIAN_SANCTUM_SARTHARION_25N     = 31311,
    RUBY_SANCTUM_HALION_10N             = 39863,
    RUBY_SANCTUM_HALION_25N             = 39864,
    RUBY_SANCTUM_HALION_10H             = 39944,
    RUBY_SANCTUM_HALION_25H             = 39945,
};

bool    RaidInfoEnable                  = false;
bool    RaidInfoMode                    = false;
bool    RaidInfoLog                     = false;
bool    RaidInfoAnnounce                = false;

int32   RaidInfoDifficulty              = 0;
// ----------------   TLK  ------------------ //
int32   RaidInfoNormalTextStandard      = 10100;
int32   RaidInfoHeroicTextStandard      = 10101;
int32   RaidInfoNormalTextSpecialToc    = 10102;
int32   RaidInfoHeroicTextSpecialToc    = 10103;
int32   RaidInfoNormalTextSpecialIcc    = 10104;
int32   RaidInfoHeroicTextSpecialIcc    = 10105;
int32   RaidInfoNormalTextGunshipIcc    = 10106;
int32   RaidInfoHeroicTextGunshipIcc    = 10107;
int32   RaidInfoNormalTextValithriaIcc  = 10108;
int32   RaidInfoHeroicTextValithriaIcc  = 10109;
// ----------------   TBC  ------------------ //
int32   RaidInfoTbcTextStandard         = 10110;

class RaidInfo_WorldScript : public WorldScript
{
    public:
        RaidInfo_WorldScript() : WorldScript("RaidInfo_WorldScript") { }

    void OnConfigLoad(bool /*reload*/)
    {
        RaidInfoEnable = sConfigMgr->GetBoolDefault("RaidInfo.Enable", false);

        if (!RaidInfoEnable)
            return;

        RaidInfoLog = sConfigMgr->GetBoolDefault("RaidInfo.Log", false);
        RaidInfoAnnounce = sConfigMgr->GetBoolDefault("RaidInfo.Announce", false);
        RaidInfoDifficulty = sConfigMgr->GetIntDefault("RaidInfo.Difficulty", 0);
    }
};

bool isElegible(Creature* creature)
{
    switch (creature->GetCreatureTemplate()->Entry)
    {
        // ----------------   TBC  ------------------ //
        case BLACK_TEMPLE_NAJENTUS:
        case BLACK_TEMPLE_SUPREMUS:
        case BLACK_TEMPLE_AKAMA:
        case BLACK_TEMPLE_TERON:
        case BLACK_TEMPLE_GURTOGG:
        case BLACK_TEMPLE_RELIQUARY:
        case BLACK_TEMPLE_MOTHER:
        case BLACK_TEMPLE_COUNCIL:
        case BLACK_TEMPLE_ILLIDAN:
        case HYJAL_RAGE_WINTERCHILL:
        case HYJAL_ANETHERON:
        case HYJAL_KAZROGAL:
        case HYJAL_AZGALOR:
        case HYJAL_ARCHIMONDE:
        case SSC_HYDROSS:
        case SSC_LURKER:
        case SSC_LEOTHERAS:
        case SSC_KARATHRESS:
        case SSC_MOROGRIM:
        case SSC_LADY_VASHJ:
        case GRUULS_LAIR_MAULGAR:
        case GRUULS_LAIR_GRUUL:
        case MAGTHERIDONS_LAIR_MAGTHERIDON:
        case KARAZHAN_ROKAD:
        case KARAZHAN_SHADIKITH:
        case KARAZHAN_HYAKISS:
        case KARAZHAN_ATTUMEN:
        case KARAZHAN_MOROES:
        case KARAZHAN_MAIDEN:
        case KARAZHAN_OPERA:
        case KARAZHAN_CURATOR:
        case KARAZHAN_TERESTIAN:
        case KARAZHAN_ARAN:
        case KARAZHAN_NETHERSPITE:
        case KARAZHAN_NIGHTBANE:
        case KARAZHAN_MALCHEZAAR:
        case SUNWELL_PLATEAU_SATHROVARR:
        case SUNWELL_PLATEAU_BRUTALLUS:
        case SUNWELL_PLATEAU_FELMYST:
        case SUNWELL_PLATEAU_ALYTHESS:
        case SUNWELL_PLATEAU_ENTROPIUS:
        case SUNWELL_PLATEAU_KILJAEDEN:
        case THE_EYE_VOID_REAPER:
        case THE_EYE_ALAR:
        case THE_EYE_SOLARIAN:
        case THE_EYE_KAELTHAS:
        // ----------------   TLK  ------------------ //
        case TRIAL_OF_THE_CRUSADER_FJOLA_10N:
        case ICC_VALANAR_10N:
        case TRIAL_OF_THE_CRUSADER_ICEHOWL_10N:
        case TRIAL_OF_THE_CRUSADER_JARAXXUS_10N:
        case TRIAL_OF_THE_CRUSADER_ANUBARAK_10N:
        case ICC_MARROWGAR_10N:
        case ICC_DEATHWHISPER_10N:
        case ICC_GUNSHIP_SKYBREAKER_10N:
        case ICC_GUNSHIP_ORGRIMS_HAMMER_10N:
        case ICC_SAURFANG_10N:
        case ICC_FESTERGUT_10N:
        case ICC_ROTFACE_10N:
        case ICC_PUTRICIDE_10N:
        case ICC_LANATHEL_10N:
        case ICC_VALITHRIA_10N:
        case ICC_SINDRAGOSA_10N:
        case ICC_LICHKING_10N:
        case NAXXRAMAS_MAEXXNA_10N:
        case NAXXRAMAS_LOATHEB_10N:
        case NAXXRAMAS_HORSEMEN_THANE_10N:
        case NAXXRAMAS_HORSEMEN_BLAUMEUX_10N:
        case NAXXRAMAS_HORSEMEN_RIVENDARE_10N:
        case NAXXRAMAS_HORSEMEN_ZELIEK_10N:
        case NAXXRAMAS_THADDIUS_10N:
        case NAXXRAMAS_KELTHUZAD_10N:
        case ONYXIAS_LAIR_ONYXIA_10N:
        case ULDUAR_LEVIATHAN_10N:
        case ULDUAR_IGNIS_10N:
        case ULDUAR_RAZORSCALE_10N:
        case ULDUAR_XT002_10N:
        case ULDUAR_BRUNDIR_10N:
        case ULDUAR_MOLGEIN_10N:
        case ULDUAR_STEELBREAKER_10N:
        case ULDUAR_KOLOGARN_10N:
        case ULDUAR_AURIAYA_10N:
        case ULDUAR_FREYA_10N:
        case ULDUAR_HODIR_10N:
        case ULDUAR_THORIM_10N:
        case ULDUAR_GENERALVEZAX_10N:
        case ULDUAR_YOGGSARON_10N:
        case ULDUAR_ALGALON_10N:
        case VAULT_OF_ARCHAVON_ARCHAVON_10N:
        case VAULT_OF_ARCHAVON_EMALON_10N:
        case VAULT_OF_ARCHAVON_KORALON_10N:
        case VAULT_OF_ARCHAVON_TORAVON_10N:
        case OBSIDIAN_SANCTUM_SARTHARION_10N:
        case RUBY_SANCTUM_HALION_10N:
        case TRIAL_OF_THE_CRUSADER_FJOLA_25N:
        case ICC_VALANAR_25N:
        case TRIAL_OF_THE_CRUSADER_ICEHOWL_25N:
        case TRIAL_OF_THE_CRUSADER_JARAXXUS_25N:
        case TRIAL_OF_THE_CRUSADER_ANUBARAK_25N:
        case ICC_MARROWGAR_25N:
        case ICC_DEATHWHISPER_25N:
        case ICC_GUNSHIP_SKYBREAKER_25N:
        case ICC_GUNSHIP_ORGRIMS_HAMMER_25N:
        case ICC_SAURFANG_25N:
        case ICC_FESTERGUT_25N:
        case ICC_ROTFACE_25N:
        case ICC_PUTRICIDE_25N:
        case ICC_LANATHEL_25N:
        case ICC_VALITHRIA_25N:
        case ICC_SINDRAGOSA_25N:
        case ICC_LICHKING_25N:
        case NAXXRAMAS_MAEXXNA_25N:
        case NAXXRAMAS_LOATHEB_25N:
        case NAXXRAMAS_HORSEMEN_THANE_25N:
        case NAXXRAMAS_HORSEMEN_BLAUMEUX_25N:
        case NAXXRAMAS_HORSEMEN_RIVENDARE_25N:
        case NAXXRAMAS_HORSEMEN_ZELIEK_25N:
        case NAXXRAMAS_THADDIUS_25N:
        case NAXXRAMAS_KELTHUZAD_25N:
        case ONYXIAS_LAIR_ONYXIA_25N:
        case EYE_OF_ETERNITY_MALYGOS:
        case ULDUAR_LEVIATHAN_25N:
        case ULDUAR_IGNIS_25N:
        case ULDUAR_RAZORSCALE_25N:
        case ULDUAR_XT002_25N:
        case ULDUAR_BRUNDIR_25N:
        case ULDUAR_MOLGEIN_25N:
        case ULDUAR_STEELBREAKER_25N:
        case ULDUAR_KOLOGARN_25N:
        case ULDUAR_AURIAYA_25N:
        case ULDUAR_FREYA_25N:
        case ULDUAR_HODIR_25N:
        case ULDUAR_MIMIRON:
        case ULDUAR_THORIM_25N:
        case ULDUAR_GENERALVEZAX_25N:
        case ULDUAR_YOGGSARON_25N:
        case ULDUAR_ALGALON_25N:
        case VAULT_OF_ARCHAVON_ARCHAVON_25N:
        case VAULT_OF_ARCHAVON_EMALON_25N:
        case VAULT_OF_ARCHAVON_KORALON_25N:
        case VAULT_OF_ARCHAVON_TORAVON_25N:
        case OBSIDIAN_SANCTUM_SARTHARION_25N:
        case RUBY_SANCTUM_HALION_25N:
        case TRIAL_OF_THE_CRUSADER_FJOLA_10H:
        case ICC_VALANAR_10H:
        case TRIAL_OF_THE_CRUSADER_ICEHOWL_10H:
        case TRIAL_OF_THE_CRUSADER_JARAXXUS_10H:
        case TRIAL_OF_THE_CRUSADER_ANUBARAK_10H:
        case ICC_MARROWGAR_10H:
        case ICC_DEATHWHISPER_10H:
        case ICC_GUNSHIP_SKYBREAKER_10H:
        case ICC_GUNSHIP_ORGRIMS_HAMMER_10H:
        case ICC_SAURFANG_10H:
        case ICC_FESTERGUT_10H:
        case ICC_ROTFACE_10H:
        case ICC_PUTRICIDE_10H:
        case ICC_LANATHEL_10H:
        case ICC_SINDRAGOSA_10H:
        case ICC_LICHKING_10H:
        case RUBY_SANCTUM_HALION_10H:
        case TRIAL_OF_THE_CRUSADER_FJOLA_25H:
        case ICC_VALANAR_25H:
        case TRIAL_OF_THE_CRUSADER_ICEHOWL_25H:
        case TRIAL_OF_THE_CRUSADER_JARAXXUS_25H:
        case TRIAL_OF_THE_CRUSADER_ANUBARAK_25H:
        case ICC_MARROWGAR_25H:
        case ICC_DEATHWHISPER_25H:
        case ICC_GUNSHIP_SKYBREAKER_25H:
        case ICC_GUNSHIP_ORGRIMS_HAMMER_25H:
        case ICC_SAURFANG_25H:
        case ICC_FESTERGUT_25H:
        case ICC_ROTFACE_25H:
        case ICC_PUTRICIDE_25H:
        case ICC_LANATHEL_25H:
        case ICC_SINDRAGOSA_25H:
        case ICC_LICHKING_25H:
        case RUBY_SANCTUM_HALION_25H:
            return true;
            break;
        default:
            return false;
            break;
    }
}
 
class RaidInfo_AllCreatureScript : public AllCreatureScript
{
    public:
        RaidInfo_AllCreatureScript() : AllCreatureScript("RaidInfo_AllCreatureScript") { }

        void AllCreatureJustDied(Creature* creature)
        {
            if (!RaidInfoEnable)
                return;

            uint32 entry = creature->GetEntry();
            if (!entry)
                return;

            if (!isElegible(creature))
                return;

            Player* recipient = creature->GetLootRecipient();
            if (!recipient)
                return;

            Map* map = creature->GetMap();
            if (!map)
                return;

            InstanceMap* iMap = map->ToInstanceMap();
            if (!map->Instanceable() || !iMap)
                return;

            InstanceScript* instance = ((InstanceMap*)iMap)->GetInstanceScript();
            if (!instance)
                return;

            uint8 spawnMode = map->GetSpawnMode();
            uint32 guildId = 0;
            bool IsGuildKill = true;
            uint32 playerCount = 0;
            std::string killTeam("");
            std::string guildName("Ninguna");
            std::string leaderName("Indefinido");
            uint32 raidId = 0;
            uint32 completedEncounters = 0;
            uint32 totalEncounters = 0;
            std::string bossName("Indefinido");
            std::string mapName("Indefinido");
            uint32 mapMaxPlayers = 0;

            spawnMode = map->GetSpawnMode();
            mapMaxPlayers = iMap ? iMap->GetMaxPlayers() : 40;
            bossName = creature->GetNameForLocaleIdx(sObjectMgr->GetDBCLocaleIndex());
            mapName = creature->GetMap()->GetMapName();
            completedEncounters = instance->GetCompletedEncountersReadable();
            totalEncounters = instance->GetTotalEncountersReadable();

            if (Group *groupInstance = recipient->GetGroup())
            {
                leaderName = groupInstance->GetLeaderName();
                for (GroupReference *itr = groupInstance->GetFirstMember(); itr != NULL; itr = itr->next())
                {
                    Player* groupMember = itr->GetSource();

                    if (!groupMember)
                        continue;

                    if (!groupMember->IsInWorld())
                        continue;

                    if (iMap->GetId() != groupMember->GetMapId())
                        continue;

                    raidId = groupMember->GetInstanceId();
                    uint32 playerGuildId = 0;
                    playerGuildId = groupMember->GetGuildId();
                    std::string playerGuildName("Ninguna");

                    if (playerGuildId != 0)
                        playerGuildName = sGuildMgr->GetGuildById(playerGuildId)->GetName();

                    playerCount++;
                    if (RaidInfoLog)
                    {
                        std::ostringstream PeopleData;
                        PeopleData << groupMember->GetGUIDLow() << DATA_SEPARATOR;
                        PeopleData << groupMember->GetName() << DATA_SEPARATOR;
                        PeopleData << playerGuildId << DATA_SEPARATOR;
                        PeopleData << playerGuildName << DATA_SEPARATOR;
                        killTeam += PeopleData.str();
                    }

                    if (IsGuildKill)
                    {
                        if (guildId == 0)
                            guildId = playerGuildId;

                        IsGuildKill = guildId != 0 && playerGuildId == guildId;
                    }
                }
            }
            else
            {
                playerCount = 1;
                guildId = recipient->GetGuildId();
                raidId = recipient->GetInstanceId();
                leaderName = recipient->GetName();
                if (guildId != 0)
                    guildName = sGuildMgr->GetGuildById(guildId)->GetName();

                IsGuildKill = guildId != 0;
                if (RaidInfoLog)
                {
                    std::ostringstream PeopleData;
                    PeopleData << recipient->GetGUIDLow() << DATA_SEPARATOR;
                    PeopleData << recipient->GetName() << DATA_SEPARATOR;
                    PeopleData << guildId << DATA_SEPARATOR;
                    PeopleData << guildName << DATA_SEPARATOR;
                    killTeam += PeopleData.str();
                }
            }

            if (guildId == 0 && IsGuildKill)
                IsGuildKill = false;

            if (!IsGuildKill)
                guildId = 0;

            if (RaidInfoAnnounce && IsGuildKill && playerCount > 1)
            {
                int32 TextIdStandard = RaidInfoNormalTextStandard;
                int32 TextIdSpecialToc = RaidInfoNormalTextSpecialToc;
                int32 TextIdSpecialIcc = RaidInfoNormalTextSpecialIcc;
                int32 TextIdGunshipIcc = RaidInfoNormalTextGunshipIcc;
                int32 TextIdValithriaIcc = RaidInfoNormalTextValithriaIcc;
                if (spawnMode == RAID_DIFFICULTY_25MAN_HEROIC || spawnMode == RAID_DIFFICULTY_10MAN_HEROIC)
                {
                    TextIdStandard = RaidInfoHeroicTextStandard;
                    TextIdSpecialToc = RaidInfoHeroicTextSpecialToc;
                    TextIdSpecialIcc = RaidInfoHeroicTextSpecialIcc;
                    TextIdGunshipIcc = RaidInfoHeroicTextGunshipIcc;
                    TextIdValithriaIcc = RaidInfoHeroicTextValithriaIcc;
                }

                if (guildId != 0)
                    guildName = sGuildMgr->GetGuildById(guildId)->GetName();

                switch (creature->GetCreatureTemplate()->Entry)
                {
                    // ----------------   TBC  ------------------ //
                case BLACK_TEMPLE_NAJENTUS:
                case BLACK_TEMPLE_SUPREMUS:
                case BLACK_TEMPLE_AKAMA:
                case BLACK_TEMPLE_TERON:
                case BLACK_TEMPLE_GURTOGG:
                case BLACK_TEMPLE_RELIQUARY:
                case BLACK_TEMPLE_MOTHER:
                case BLACK_TEMPLE_COUNCIL:
                case BLACK_TEMPLE_ILLIDAN:
                case HYJAL_RAGE_WINTERCHILL:
                case HYJAL_ANETHERON:
                case HYJAL_KAZROGAL:
                case HYJAL_AZGALOR:
                case HYJAL_ARCHIMONDE:
                case SSC_HYDROSS:
                case SSC_LURKER:
                case SSC_LEOTHERAS:
                case SSC_KARATHRESS:
                case SSC_MOROGRIM:
                case SSC_LADY_VASHJ:
                case GRUULS_LAIR_MAULGAR:
                case GRUULS_LAIR_GRUUL:
                case MAGTHERIDONS_LAIR_MAGTHERIDON:
                case KARAZHAN_ROKAD:
                case KARAZHAN_SHADIKITH:
                case KARAZHAN_HYAKISS:
                case KARAZHAN_ATTUMEN:
                case KARAZHAN_MOROES:
                case KARAZHAN_MAIDEN:
                case KARAZHAN_OPERA:
                case KARAZHAN_CURATOR:
                case KARAZHAN_TERESTIAN:
                case KARAZHAN_ARAN:
                case KARAZHAN_NETHERSPITE:
                case KARAZHAN_NIGHTBANE:
                case KARAZHAN_MALCHEZAAR:
                case SUNWELL_PLATEAU_SATHROVARR:
                case SUNWELL_PLATEAU_BRUTALLUS:
                case SUNWELL_PLATEAU_FELMYST:
                case SUNWELL_PLATEAU_ALYTHESS:
                case SUNWELL_PLATEAU_ENTROPIUS:
                case SUNWELL_PLATEAU_KILJAEDEN:
                case THE_EYE_VOID_REAPER:
                case THE_EYE_ALAR:
                case THE_EYE_SOLARIAN:
                case THE_EYE_KAELTHAS:
                    sWorld->SendWorldText(RaidInfoTbcTextStandard, mapName.c_str(), bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                    // ----------------   TLK  ------------------ //
                case TRIAL_OF_THE_CRUSADER_FJOLA_10N:
                    if (RaidInfoDifficulty <= 0)
                        sWorld->SendWorldText(TextIdSpecialToc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALANAR_10N:
                    if (RaidInfoDifficulty <= 0)
                        sWorld->SendWorldText(TextIdSpecialIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_GUNSHIP_SKYBREAKER_10N:
                case ICC_GUNSHIP_ORGRIMS_HAMMER_10N:
                    if (RaidInfoDifficulty <= 0)
                        sWorld->SendWorldText(TextIdGunshipIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_ICEHOWL_10N:
                case TRIAL_OF_THE_CRUSADER_JARAXXUS_10N:
                case TRIAL_OF_THE_CRUSADER_ANUBARAK_10N:
                case ICC_MARROWGAR_10N:
                case ICC_DEATHWHISPER_10N:
                case ICC_SAURFANG_10N:
                case ICC_FESTERGUT_10N:
                case ICC_ROTFACE_10N:
                case ICC_PUTRICIDE_10N:
                case ICC_LANATHEL_10N:
                case ICC_SINDRAGOSA_10N:
                case ICC_LICHKING_10N:
                case NAXXRAMAS_MAEXXNA_10N:
                case NAXXRAMAS_LOATHEB_10N:
                case NAXXRAMAS_HORSEMEN_THANE_10N:
                case NAXXRAMAS_HORSEMEN_BLAUMEUX_10N:
                case NAXXRAMAS_HORSEMEN_RIVENDARE_10N:
                case NAXXRAMAS_HORSEMEN_ZELIEK_10N:
                case NAXXRAMAS_THADDIUS_10N:
                case NAXXRAMAS_KELTHUZAD_10N:
                case ONYXIAS_LAIR_ONYXIA_10N:
                case ULDUAR_LEVIATHAN_10N:
                case ULDUAR_IGNIS_10N:
                case ULDUAR_RAZORSCALE_10N:
                case ULDUAR_XT002_10N:
                case ULDUAR_BRUNDIR_10N:
                case ULDUAR_MOLGEIN_10N:
                case ULDUAR_STEELBREAKER_10N:
                case ULDUAR_KOLOGARN_10N:
                case ULDUAR_AURIAYA_10N:
                case ULDUAR_FREYA_10N:
                case ULDUAR_HODIR_10N:
                case ULDUAR_THORIM_10N:
                case ULDUAR_GENERALVEZAX_10N:
                case ULDUAR_YOGGSARON_10N:
                case ULDUAR_ALGALON_10N:
                case VAULT_OF_ARCHAVON_ARCHAVON_10N:
                case VAULT_OF_ARCHAVON_EMALON_10N:
                case VAULT_OF_ARCHAVON_KORALON_10N:
                case VAULT_OF_ARCHAVON_TORAVON_10N:
                case OBSIDIAN_SANCTUM_SARTHARION_10N:
                case RUBY_SANCTUM_HALION_10N:
                    if (RaidInfoDifficulty <= 0)
                        sWorld->SendWorldText(TextIdStandard, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_FJOLA_25N:
                    if (RaidInfoDifficulty <= 1)
                        sWorld->SendWorldText(TextIdSpecialToc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALANAR_25N:
                    if (RaidInfoDifficulty <= 1)
                        sWorld->SendWorldText(TextIdSpecialIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_GUNSHIP_SKYBREAKER_25N:
                case ICC_GUNSHIP_ORGRIMS_HAMMER_25N:
                    if (RaidInfoDifficulty <= 1)
                        sWorld->SendWorldText(TextIdGunshipIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_ICEHOWL_25N:
                case TRIAL_OF_THE_CRUSADER_JARAXXUS_25N:
                case TRIAL_OF_THE_CRUSADER_ANUBARAK_25N:
                case ICC_MARROWGAR_25N:
                case ICC_DEATHWHISPER_25N:
                case ICC_SAURFANG_25N:
                case ICC_FESTERGUT_25N:
                case ICC_ROTFACE_25N:
                case ICC_PUTRICIDE_25N:
                case ICC_LANATHEL_25N:
                case ICC_SINDRAGOSA_25N:
                case ICC_LICHKING_25N:
                case NAXXRAMAS_MAEXXNA_25N:
                case NAXXRAMAS_LOATHEB_25N:
                case NAXXRAMAS_HORSEMEN_THANE_25N:
                case NAXXRAMAS_HORSEMEN_BLAUMEUX_25N:
                case NAXXRAMAS_HORSEMEN_RIVENDARE_25N:
                case NAXXRAMAS_HORSEMEN_ZELIEK_25N:
                case NAXXRAMAS_THADDIUS_25N:
                case NAXXRAMAS_KELTHUZAD_25N:
                case ONYXIAS_LAIR_ONYXIA_25N:
                case EYE_OF_ETERNITY_MALYGOS:
                case ULDUAR_LEVIATHAN_25N:
                case ULDUAR_IGNIS_25N:
                case ULDUAR_RAZORSCALE_25N:
                case ULDUAR_XT002_25N:
                case ULDUAR_BRUNDIR_25N:
                case ULDUAR_MOLGEIN_25N:
                case ULDUAR_STEELBREAKER_25N:
                case ULDUAR_KOLOGARN_25N:
                case ULDUAR_AURIAYA_25N:
                case ULDUAR_FREYA_25N:
                case ULDUAR_HODIR_25N:
                case ULDUAR_MIMIRON:
                case ULDUAR_THORIM_25N:
                case ULDUAR_GENERALVEZAX_25N:
                case ULDUAR_YOGGSARON_25N:
                case ULDUAR_ALGALON_25N:
                case VAULT_OF_ARCHAVON_ARCHAVON_25N:
                case VAULT_OF_ARCHAVON_EMALON_25N:
                case VAULT_OF_ARCHAVON_KORALON_25N:
                case VAULT_OF_ARCHAVON_TORAVON_25N:
                case OBSIDIAN_SANCTUM_SARTHARION_25N:
                case RUBY_SANCTUM_HALION_25N:
                    if (RaidInfoDifficulty <= 1)
                        sWorld->SendWorldText(TextIdStandard, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_FJOLA_10H:
                    if (RaidInfoDifficulty <= 2)
                        sWorld->SendWorldText(TextIdSpecialToc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALANAR_10H:
                    if (RaidInfoDifficulty <= 2)
                        sWorld->SendWorldText(TextIdSpecialIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_GUNSHIP_SKYBREAKER_10H:
                case ICC_GUNSHIP_ORGRIMS_HAMMER_10H:
                    if (RaidInfoDifficulty <= 2)
                        sWorld->SendWorldText(TextIdGunshipIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALITHRIA_10N:
                    if (RaidInfoDifficulty <= 2)
                        sWorld->SendWorldText(TextIdValithriaIcc, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_ICEHOWL_10H:
                case TRIAL_OF_THE_CRUSADER_JARAXXUS_10H:
                case TRIAL_OF_THE_CRUSADER_ANUBARAK_10H:
                case ICC_MARROWGAR_10H:
                case ICC_DEATHWHISPER_10H:
                case ICC_SAURFANG_10H:
                case ICC_FESTERGUT_10H:
                case ICC_ROTFACE_10H:
                case ICC_PUTRICIDE_10H:
                case ICC_LANATHEL_10H:
                case ICC_SINDRAGOSA_10H:
                case ICC_LICHKING_10H:
                case RUBY_SANCTUM_HALION_10H:
                    if (RaidInfoDifficulty <= 2)
                        sWorld->SendWorldText(TextIdStandard, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_FJOLA_25H:
                    if (RaidInfoDifficulty <= 3)
                        sWorld->SendWorldText(TextIdSpecialToc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALANAR_25H:
                    if (RaidInfoDifficulty <= 3)
                        sWorld->SendWorldText(TextIdSpecialIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_GUNSHIP_SKYBREAKER_25H:
                case ICC_GUNSHIP_ORGRIMS_HAMMER_25H:
                    if (RaidInfoDifficulty <= 3)
                        sWorld->SendWorldText(TextIdGunshipIcc, mapName.c_str(), mapMaxPlayers, guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case ICC_VALITHRIA_25N:
                    if (RaidInfoDifficulty <= 3)
                        sWorld->SendWorldText(TextIdValithriaIcc, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                case TRIAL_OF_THE_CRUSADER_ICEHOWL_25H:
                case TRIAL_OF_THE_CRUSADER_JARAXXUS_25H:
                case TRIAL_OF_THE_CRUSADER_ANUBARAK_25H:
                case ICC_MARROWGAR_25H:
                case ICC_DEATHWHISPER_25H:
                case ICC_SAURFANG_25H:
                case ICC_FESTERGUT_25H:
                case ICC_ROTFACE_25H:
                case ICC_PUTRICIDE_25H:
                case ICC_LANATHEL_25H:
                case ICC_SINDRAGOSA_25H:
                case ICC_LICHKING_25H:
                case RUBY_SANCTUM_HALION_25H:
                    if (RaidInfoDifficulty <= 3)
                        sWorld->SendWorldText(TextIdStandard, mapName.c_str(), mapMaxPlayers, bossName.c_str(), guildName.c_str(), playerCount, mapMaxPlayers, completedEncounters, totalEncounters, raidId);
                    break;
                default:
                    break;
                }
            }

            if (!RaidInfoLog)
                return;

            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_RAIDINFO);
            stmt->setUInt32(0, guildId);
            stmt->setString(1, guildName.c_str());
            stmt->setUInt32(2, entry);
            stmt->setUInt32(3, playerCount);
            stmt->setString(4, leaderName.c_str());
            stmt->setUInt32(5, raidId);
            stmt->setString(6, killTeam.c_str());
            stmt->setUInt32(7, spawnMode);
            stmt->setUInt32(8, map->GetId());
            stmt->setString(9, bossName.c_str());
            stmt->setString(10, mapName.c_str());
            stmt->setUInt32(11, mapMaxPlayers);
            stmt->setUInt32(12, completedEncounters);
            stmt->setUInt32(13, totalEncounters);

            CharacterDatabase.Execute(stmt);
        }
};

void AddSC_raidinfoannounce()
{
    new RaidInfo_AllCreatureScript();
    new RaidInfo_WorldScript();
}
